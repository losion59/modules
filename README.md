# modules

**Oauth Board**

     spring boot와 spring securitiy 및 spring oauth를 활용하여 로그인 시스템을 구축하였습니다. 또한 python으로 웹 크롤링을 하여 데이터를 AWS RDS MARIA DB에 저장하고
     저장한 데이터를 기계학습을 위해 데이터를 정제한는 웹 앱을 포함하고 있습니다.
    
**front**

     webpack을 적용하여 ecma6,vue 및 scss를 트랜스파일 및 통합한 front 파일(html,js,css)을 백엔드 서버에 배포하기 위해 작업한 모듈입니다. 
     js 트랜스 파일 도구는 babel을 사용하였고, css는 scss 및 auto prefixer를 사용하여 생산성을 높였습니다.또한 html-webpack-plugin으로 관련된 js,css를 자동 통합하여 백엔드 
    서버내에 배포하였습니다. 이들의 설정인 webpack.config.js의 주요 설정들을 /config 폴더에 각각 모듈링하여 사용하였습니다.
     UI framework는 vue.js를 사용하였고 jquery의 개입을 최대한 지양하였습니다. ajax는 axios를 사용하였고, animation은 Velocity를 사용하였습니다. 또한 ecma6의 
    module(import,export) 기능을 최대한 활용하여 기존은 난잡했던 js관리를 개선하였고, class 및 promise를 사용하여 가독성 향상 및 유지보수 관리성을 높였습니다. 
     

**web_crawling**

     python의 selenium 라이브러리를 활용한 웹 크롤링 모듈입니다. 기존 http 통신을 통한 크롤링은 특정 이미지의 개수만 가져올 수 있어 chrome driver를 사용하여 headless browser를 
    통해 원하는 만큼의 이미지를 naver, bing, google에서 가지고 올 수 있습니다. 