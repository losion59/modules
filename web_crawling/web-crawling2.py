import sys

sys.path.extend(['/Users/otdealmac4/PycharmProjects/web_crawling', '/Users/otdealmac4/PycharmProjects/web_crawling', '/usr/local/anaconda3/envs/init/lib/python37.zip', '/usr/local/anaconda3/envs/init/lib/python3.7', '/usr/local/anaconda3/envs/init/lib/python3.7/lib-dynload', '/usr/local/anaconda3/envs/init/lib/python3.7/site-packages'])

from web_crawler import web_crawler
import re
import argparse

import pandas as pd

from bs4 import BeautifulSoup
from urllib import parse

import pymysql

parser = argparse.ArgumentParser(description="type end of your category")
parser.add_argument("--end", required=True)
parser.add_argument("--host", required=True)
parser.add_argument("--username", required=True)
parser.add_argument("--password", required=True)
parser.add_argument("--db", required=True)

args = parser.parse_args()
end = int(args.end)

#category from excel
search_args = pd.read_excel("/Users/otdealmac4/PycharmProjects/web_crawling/search_args.xlsx")
categorys = list(search_args.columns)

sql = "insert into unrefineded({columns}) values({values})"
columns = ""
values = ""

for idx, category in enumerate(categorys):
    if idx < end:
        columns += category+","
        values += "\"{"+category+"}\","

sql = sql.format(columns=columns[:-1], values=values[:-1])

#init crawler
web_crawler = web_crawler()
#init regExp
url_censor = re.compile(r"http\S+[.](bmp|tif|gif|png|jpg|jpeg)")
#init db
conn = pymysql.connect(host=args.host,
                       user=args.username,
                       password=args.password,
                       db=args.db,
                       charset="utf8mb4")
cursor = conn.cursor()
cursor.execute("delete from unrefineded")

#refining
rows = search_args.iterrows()
id = 0
try:
    for idx, row in rows:
        amount_per = int(row["num_per"])
        for keyword in row["keyword"].split(","):
            page_src = web_crawler.get_page_src(keyword)
            soup = BeautifulSoup(str(page_src), "html.parser")
            a_tags = soup.find_all("a", class_="rg_l")
            if len(a_tags) > amount_per:
                a_tags = a_tags[:amount_per]

            for a_tag in a_tags:
                for attr in str(a_tag).split(" "):
                    if(attr.startswith("href")):
                        try:
                            tmp_url = url_censor.search(attr).group()
                            url = parse.unquote(tmp_url)
                            cursor.execute(sql.format(**row[1:end-2],keyword=keyword,url=url, id=id))
                            id+=1
                        except Exception as e:
                            print(e)
                            pass #href="#"
            conn.commit()
finally:
    web_crawler.close()
    conn.close()
