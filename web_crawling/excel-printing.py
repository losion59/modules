import sys
sys.path.extend(['/Users/otdealmac4/PycharmProjects/web_crawling', '/Users/otdealmac4/PycharmProjects/web_crawling', '/usr/local/anaconda3/envs/init/lib/python37.zip', '/usr/local/anaconda3/envs/init/lib/python3.7', '/usr/local/anaconda3/envs/init/lib/python3.7/lib-dynload', '/usr/local/anaconda3/envs/init/lib/python3.7/site-packages'])

import openpyxl
import pymysql
import pandas as pd

import argparse
from util import env

parser = argparse.ArgumentParser(description="type end of your category")
parser.add_argument("--sheet", required=True)
parser.add_argument("--limit", required=True)

parser.add_argument("--host", required=True)
parser.add_argument("--username", required=True)
parser.add_argument("--password", required=True)
parser.add_argument("--db", required=True)


args = parser.parse_args()

conn = pymysql.connect(host=args.host,
                       user=args.username,
                       password=args.password,
                       db=args.db,
                       charset="utf8mb4")

cursor = conn.cursor()

#category from excel
config = { "working_directory" : env.curr_path }
table_name = "unrefineded"

search_args = pd.read_excel("{working_directory}/prop/search_args.xlsx".format(**config),sheet_name=table_name)
categorys = list(search_args.columns)[1:]

sql = "select {columns} from unrefineded limit 400"
columns = ""

for category in categorys:
        columns += category+","

sql = sql.format(columns=columns[:-1])

cursor.execute(sql)
rows = cursor.fetchall()

pending_excels = openpyxl.load_workbook("{working_directory}/refined_chunk.xlsx".format(**config))
active_excel = pending_excels[args.sheet]

id = 1
if active_excel.max_row != 1:
    id = active_excel.max_row + 1


try:
    for row in rows:
        row = list(row)
        row.insert(0, id)
        active_excel.append(row)
        id += 1
finally:
    pending_excels.save("/Users/otdealmac4/PycharmProjects/web_crawling/refined_chunk.xlsx")
    conn.close()
