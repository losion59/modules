import os
import time
import json
import ssl

from util import env
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class web_crawler():

    def __init__(self):
        options = webdriver.ChromeOptions()
        options.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")
        # options.add_argument('headless')
        # options.add_argument('disable-gpu')
        # options.add_argument('window-size=1920x1080')

        ssl._create_default_https_context = ssl._create_unverified_context

        config = {"working_directory": env.curr_path}
        self.browser = webdriver.Chrome("{working_directory}/chromedriver".format(**config), options=options)
        self.engine_list = {}
        with open('{working_directory}/prop/crawling.json'.format(**config)) as crawling_json:
            crawling_list = json.load(crawling_json)
            for engine in crawling_list.keys():
                self.engine_list[engine] = str(crawling_list[engine]["url"])

        #self.init()

    def get_page_src(self, query, engine, **config):
        #self.browser.get(self.engine_list[engine].format(query=query))
        self.browser.get("https://www.google.co.kr/search?hl=ko&tbs=simg:CAESsAIJQIqn6vwFwfgapAILELCMpwgaYgpgCAMSKOAK4QqZFYENlxXiCoUNgg2YFdgFjjaNNrAhkjaUNpM2-ifWKvk1xyEaMFVCTdFE1hzSLajdihEYazqYrkbn7MrIDLhr88ImTi2Mzo8qhhZFWUN3ZoizfU6bJSAEDAsQjq7-CBoKCggIARIE2f24yQwLEJ3twQkanAEKIgoPaW50ZXJpb3IgZGVzaWdu2qWI9gMLCgkvbS8wMnJmZHEKGgoHY2VpbGluZ9qliPYDCwoJL20vMDNnZnNwChcKBWZsb29y2qWI9gMKCggvbS8wbDdfOAoeCgtsaXZpbmcgcm9vbdqliPYDCwoJL20vMDNmNnRxCiEKDXdvb2QgZmxvb3JpbmfapYj2AwwKCi9tLzAyN3hobTAM&sxsrf=ACYBGNQXYdjEQNiDlo0KOHcImoQiO8Lf4w:1573668972768&q=interior+design&tbm=isch&sa=X&ved=2ahUKEwiUqPbg5eflAhWQHqYKHVPAAu4Qsw56BAgCEAE&biw=1631&bih=817")
        timeout = 3.5
        raise Exception
        if config['required']:
            self.browser.find_element_by_css_selector(config['login_btn']).click()
            time.sleep(0.5)
            self.browser.find_element_by_css_selector(config['google_btn']).click()
            self.browser.get(self.engine_list[engine].format(query=query))

        body = self.browser.find_element_by_tag_name("body")

        for i in range(config['trial']):
            try:
                body.send_keys(Keys.PAGE_DOWN)
                time.sleep(0.2)
            except:
                body = self.browser.find_element_by_tag_name("body")

            try:
                self.browser.find_element_by_css_selector(config['next_btn']).click()
            except:
                pass

        time.sleep(timeout)

        source = self.browser.page_source
        return source

    def init(self):
        self.browser.get("https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin")
        id_input = self.browser.find_element_by_id("identifierId")
        id_input.send_keys("losion59")
        next_btn = self.browser.find_element_by_id("identifierNext")
        next_btn.click()

        time.sleep(1.5)

        pw_input = self.browser.find_element_by_css_selector("input.whsOnd.zHQkBf")
        pw_input.send_keys("syb1524ko7")
        next_btn = self.browser.find_element_by_id("passwordNext")
        next_btn.click()

        time.sleep(2)

    def close(self):
        self.browser.close()