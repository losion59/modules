import re

class html_processor:

    def __init__(self):
        pass

    compiler = ""

    @classmethod
    def refine_html(cls,page_src):
        print(cls.find_tags("img",page_src))
        pass

    @classmethod
    def find_tags(cls,tag,page_src):

        if tag in ["img"]:
            cls.compiler = r"(?:<{tag}(?:\S|\s)*>)".format(tag=tag)
        else:
            cls.compiler = r"(?:<{tag}(\S|\s)*>(\S|\s)*</{tag}>)".format(tag=tag)

        return len([x.group() for x in re.finditer(cls.compiler, page_src)])