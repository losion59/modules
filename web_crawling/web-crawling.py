import sys
sys.path.extend(['/Users/otdealmac4/PycharmProjects/web_crawling', '/Users/otdealmac4/PycharmProjects/web_crawling', '/usr/local/anaconda3/envs/init/lib/python37.zip', '/usr/local/anaconda3/envs/init/lib/python3.7', '/usr/local/anaconda3/envs/init/lib/python3.7/lib-dynload', '/usr/local/anaconda3/envs/init/lib/python3.7/site-packages'])

import pandas as pd
import json
import re
import argparse
import pymysql
from sqlalchemy import create_engine
import copy
import multiprocessing

from bs4 import BeautifulSoup
from urllib import parse

from util import (env)
from util.html_processor import html_processor
from web_crawler import web_crawler


parser = argparse.ArgumentParser(description="type end of your category")
parser.add_argument("--limit", required=True)
parser.add_argument("--sheet", required=True)

parser.add_argument("--host", required=True)
parser.add_argument("--username", required=True)
parser.add_argument("--password", required=True)
parser.add_argument("--db", required=True)


args = parser.parse_args()

#code json 순회
config = { "working_directory" : env.curr_path}
code_list = ""

with open('{working_directory}/prop/code.json'.format(**config)) as code_json:
    code_list = json.load(code_json)


def define_code(code_list,code,limit=100,category=""):
    for key in code_list.keys():
        if code == key:
            category += code_list[key]['name']
            return category
        elif int(code) - int(key) < limit and int(code) - int(key) > 0 :
            category += code_list[key]['name'] + "-"
            children = code_list[key]['children']
            return define_code(children,code,limit/10,category)


#init crawler
web_crawler = web_crawler()
#init regExp
url_censor = re.compile(r"http\S+\"")
param_censor = re.compile(r"http\S+[.](bmp|tif|png|jpg|jpeg)")
#init db
ip = "mysql+mysqldb://{username}:{password}@{host}/{db}".format(host=args.host,username=args.username,password=args.password,db=args.db)
conn = pymysql.connect(host=args.host,
                       user=args.username,
                       password=args.password,
                       db=args.db,
                       charset="utf8mb4")
cursor = conn.cursor()
cursor.execute("delete from unrefineded")

#excel 처리
table_name = "unrefineded"
search_args = pd.read_excel("{working_directory}/prop/search_args.xlsx".format(**config), sheet_name=table_name)

columns = list(search_args.columns)
limit = int(args.limit)

sql = "insert into {table}({columns}) values({values})"
id = 0
count = 0

try:
    for idx, row in search_args.iterrows():

        line_template = {}
        dumb = ""
        columns_name = ""
        values_name = ""

        keyword_main = define_code(code_list, str(row["category"]))

        for idx,column in enumerate(columns):
            if pd.isnull(row[column]):
                line_template[column] = dumb
                dumb = idx*"-"
            else:
                line_template[column] = row[column]
            columns_name += column+","
            values_name += "\"{" + column + "}\","

        sql = sql.format(table=table_name, columns=columns_name[:-1], values=values_name[:-1])


        for keyword in row["product_name"].split(","):

            crawling_list = ""
            with open('{working_directory}/prop/crawling.json'.format(**config)) as crawling_json:
                crawling_list = json.load(crawling_json)

            for engine in crawling_list.keys():

                page_src = web_crawler.get_page_src(keyword, engine, **crawling_list[engine])

                soup = BeautifulSoup(str(page_src), "html.parser")
                a_tags = soup.select(crawling_list[engine]["selector"])

                if len(a_tags) > limit:
                    a_tags = a_tags[:limit]
                for a_tag in a_tags:
                    meta = ""

                    if engine in ["bing"]:
                        meta = json.loads(a_tag[crawling_list[engine]['meta']])
                    elif engine in ["daum"]:
                        meta = { crawling_list[engine]['attr'] : a_tag[crawling_list[engine]['meta']] }
                    else:
                        meta = json.loads(a_tag.text)

                    tmp = meta[crawling_list[engine]['attr']]

                    url = parse.unquote(tmp)
                    line = copy.deepcopy(line_template)

                    line['url'] = url
                    line['product_name'] = keyword
                    line['no'] = id
                    line['category'] = keyword_main
                    line['weight'] = 100
                    id += 1
                    try:
                        cursor.execute(sql.format(**line))
                    except:
                        print(sql.format(**line))


                    """"
                    for attr in str(a_tag).split(" "):
                        if attr.startswith(crawling_list[engine]["attr"]):
                            try:
                                line = copy.deepcopy(line_template)
                                tmp_url = url_censor.search(attr).group()[:-1]

                                if tmp_url.strip().endswith("uact=8"):
                                    tmp_url = param_censor.search(tmp_url).group()
                    """


finally:
    print(count)
    #web_crawler.close()
    conn.commit()
    conn.close()




