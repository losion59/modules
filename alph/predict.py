# -*- coding: utf-8 -*-
"""
Created on Thu Oct 11 17:47:01 2018

@author: Josh
"""

import pandas as pd
from tools import make_path, data_read, weight_resetter, stripper, file_format_parser, separating_jpg_predict, file_writer
import glob
import os
from keras.models import model_from_json
from datetime import datetime
import urllib.request as request
from tools import *
from tqdm import tqdm


idx = []
"""
def sheet_merger(data, sheet_names, heads, var_names):
    initial_sheet_names = sheet_names[0]
    dataFrame = data[initial_sheet_names]    
    dataFrame = dataFrame.groupby(by=['이미지경로']).sum()
    merged = pd.DataFrame()
    
    merged['이미지경로'] = dataFrame.index    
      
    for v in var_names:
        for sub_element in heads[v]:
            a = dataFrame[sub_element] >= 1
            a = a.astype('int')
            merged[sub_element] = list(a)
    
    return(merged)


def variable_sorter(dataframe_path, the_latest_var):
    
    data, sheet_names = data_read(dataframe_path)
    data, sheet_names, weight_names = weight_resetter(data, sheet_names)
    var_names = ['카테고리', '사이즈', '업체', '컬러']
    data, sheet_names, heads = stripper(data, sheet_names, var_names)
    new_data = sheet_merger(data, sheet_names, heads, var_names)
    variables = pd.read_excel(the_latest_var, index_col=0)
    final_data = pd.DataFrame()
    final_data['이미지경로'] = new_data['이미지경로']
    new_data = new_data.reset_index()
    variables = list(variables.iloc[:,0])
    
    for v in variables:
        try:
            final_data[v] = new_data[v].astype('int')
        except:
            final_data[v] = int(0)
            
    return(final_data)
        
def load_latest_model():
    print("Start loading model...")
    source = "model_weights"
    weights_list = list(glob.glob(os.path.join(source, '*.h5')))
    weight_file = max(weights_list, key=os.path.getctime)
    
    models_list = list(glob.glob(os.path.join(source, '*.json')))
    model_file = max(models_list, key = os.path.getctime)
    
    model_json_file = open(model_file, 'r')
    loaded_model_from_json = model_json_file.read()
    model_json_file.close()
    loaded_model = model_from_json(loaded_model_from_json)
    
    loaded_model.load_weights(weight_file)
    print("model loaded.")
    return(loaded_model)

def img_downloader(new_data):
    print("Downloading Images...")
    for i in tqdm(new_data.index, desc="Downloading Image Files:"):
        try:
            name = file_format_parser(new_data, i)
            print(name,"\n");
            #make_path("pred_temp_img")
            new_name = "pred_temp_img/" + str(i) + name
            #request.urlretrieve(new_data["이미지경로"][i], new_name)
            file_writer(new_name, new_data, i)
            #request.URLopener.retrieve(new_data["이미지경로"][i], new_name)
            #print("image: " + str(i) + " - " + new_name)
        except Exception as e:
            print(e)
            pass
"""   
#%%
def alphago_predict():
    pred_folder = "pred_data"
    data_files = list(glob.glob(os.path.join(pred_folder, '*.xlsx')))
    print("=="+pred_folder)
    print(data_files)
    print("\n")
    the_latest = max(data_files, key = os.path.getctime)
    print("==the_latest")
    print(the_latest)
    print("\n")
    var_folder = "Variable_Names/"
    var_files = list(glob.glob(os.path.join(var_folder, '*.xlsx')))
    print("==var_files")
    print(var_files)
    print("\n")
    the_latest_var = max(var_files, key = os.path.getctime)
    print("==the_latest_var")
    print(the_latest_var)
    print("\n")
    sheet_folder = "Sheet_Names"
    sheet_files = list(glob.glob(os.path.join(sheet_folder, '*.xlsx')))
    print("==sheet_files")
    print(sheet_files)
    print("\n")
    the_latest_sheet = max(sheet_files, key = os.path.getctime)
    #print("==the_latest_sheet")
    #print(the_latest_sheet)
    #print("\n")
    
    data = variable_sorter(the_latest, the_latest_var)
    print(data)
    print("=== img size ===")
    print(len(data))
    ## to lpoad and write images
    now = datetime.now().strftime('%Y%m%d_%H%M%S')
    img_downloader(data)
    
    source = "pred_temp_img/"
    dest = "pred_img/" + now +"/"
    data2 = separating_jpg_predict(data, source, dest, data['이미지경로'])
    X_img = image_loader(data, dest)
    X = data[data['cleaned'] == 1]
    X = X.drop(['cleaned', '이미지경로'], axis = 1)
    
    loaded_model = load_latest_model()
    
    pred = loaded_model.predict([X, X_img])
    #pred[pred >= 0.5] = 1
    #pred[pred < 0.5] = 0
    
    ## Saving Predicted Values
    pred_dataFrame = pd.DataFrame()
    data2 = data2.reset_index(drop=True)
    #print("==================data2: ", data2)
    pred_dataFrame['이미지경로'] = data2
    
    #for i in range(len(data['이미지경로'])) :
    #    for j in range(len(idx)) :
    #        if i == idx :
    #            pred_dataFrame['이미지경로'] = pred_dataFrame['이미지경로'].drop(j)
    #print("==================data['이미지경로']: ", pred_dataFrame['이미지경로'])
    
    #print("==================pred_dataFrame: ", pred_dataFrame)
    the_latest_sheet = pd.read_excel(the_latest_sheet)
    the_latest_sheet = list(the_latest_sheet['sheet_names'])
    pred = pd.DataFrame(pred)
    pred = round(pred, 4)
    pred = pred.astype('float')
    #print("==================pred: ",pred)
    
    #idx = image_loader(data, dest).get('idx')
    #for i in range(len(the_latest_sheet)):
    #    for j in range(len(idx)):
    #        print("================== i: ",i,"\n")
    #        if  i == int(idx[j]):
    #            print("==================idx: ",idx[j],"\n")
    #            pred_n = the_latest_sheet[int(idx[j])]
    #            pred_dataFrame[pred_n] = pred.iloc[:,int(idx[j])]
                
    for i in range(len(the_latest_sheet)):
        pred_n = the_latest_sheet[i]
        pred_dataFrame[pred_n] = pred.iloc[:,i]            
         
    pred_dataFrame.columns.values[0] = "img_link"
    #pred_dataFrame.columns.values[1] = "img_link"  
    
    final_name = "prediction/prediction_" + now + ".csv"
    make_path(final_name)
    
    pred_dataFrame.to_csv(final_name)














