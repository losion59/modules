
import sys

import pandas as pd
import numpy as np
import os
import glob
import errno
import shutil
import time
from datetime import datetime
from urllib import request
import imghdr
from PIL import Image
#import wget
from tqdm import tqdm
from time import sleep 
import random
import urllib
from io import BytesIO
import requests


import pandas as pd
import numpy as np
import cv2
from keras.models import Sequential, Model, model_from_json
from keras.layers import ELU # Loss layer
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers.core import Activation, Flatten, Dropout, Dense
from keras.preprocessing.image import img_to_array
from keras.layers.merge import Concatenate
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, TensorBoard
from keras import regularizers
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, recall_score
import glob
import errno

import tensorflow as tf
import random

var_names = ['카테고리', '사이즈', '업체','컬러']

## Writing permission on file
def make_path(f_name):
    if not os.path.exists(os.path.dirname(f_name)):
        try:
            os.makedirs(os.path.dirname(f_name))
        except OSError as exc:
            if exc.errno != errno.EXXIST:
                raise
                
                
# loading data into dict_dataframe
def data_read(data):
    
    df = pd.read_excel(data, sheet_name = None)
    sheet_names = list(df.keys())

#    print("======df======"+"\n");
#    print(df);
#    print("\n");
#    print("======df======"+"\n");
#    print("======sheet_names======"+"\n");
#    print(sheet_names);
#    print("\n");
#    print("======sheet_names======"+"\n");
    
    return(df, sheet_names)
    

## Sheet names to variables
def weight_resetter(data, sheet_names):
    
    weight_names = []
    
    for sheet in sheet_names:
        
        data[sheet][sheet] = int(1)
        data[sheet].rename(columns = {'가중치': sheet + '_가중치'}, inplace = True)
        weight_names.append(sheet)
        weight_names.append(sheet + '_가중치')
        
    return(data, sheet_names, weight_names)
    

## parsing element and making dummy variables
def stripper(data, sheet_names, var_names):
    
    v = {}
    
    for var_name in var_names:
        v[var_name] = pd.Series()
    
    for sheet in sheet_names:

        for var_name in var_names:
            t = data[sheet][var_name].str.split(',').apply(pd.Series, 1).stack()
            v[var_name] = pd.concat([v[var_name], t], axis = 0) 
            t = pd.get_dummies(t)
            t = t.sum(level = 0)
            data[sheet] = pd.concat([data[sheet], t], axis = 1)
            
    for var_name in var_names:
        v[var_name] = sorted(list(pd.unique(v[var_name])))
            
    return(data, sheet_names, v)
    
idx = []
    
def sheet_merger(data, sheet_names, heads, weight_names):
    
    initial_sheet_names = sheet_names[0]
    temp = list(sheet_names)
    del(temp[0])

    dataFrame = data[initial_sheet_names]

    for t in temp:        
        dataFrame = dataFrame.append(data[t], ignore_index = True, sort = False)

    dataFrame = dataFrame.drop_duplicates(["이미지경로"], keep="last")
    dataFrame = dataFrame.reset_index()
    print(dataFrame)
    dataFrame['번호'] = range(len(dataFrame.index))
    print(dataFrame)
    merged = pd.DataFrame()
    merged['이미지경로'] = dataFrame['이미지경로']

    for w in weight_names:
        merged[w] = dataFrame[w]

    for v in var_names:
        for sub_element in heads[v]:
            a = dataFrame[sub_element] >= 1
            a = a.astype('int')
            merged[sub_element] = list(a)
    # index = np.array(len(dataFrame))
    # merged.reindex(index)
    print(merged)
    return(merged, sheet_names)


def sheet_mergers(data, sheet_names, heads, var_names):
    initial_sheet_names = sheet_names[0]
    dataFrame = data[initial_sheet_names]
    dataFrame = dataFrame.groupby(by=['이미지경로']).sum()
    merged = pd.DataFrame()

    merged['이미지경로'] = dataFrame.index

    for v in var_names:
        for sub_element in heads[v]:
            a = dataFrame[sub_element] >= 1
            a = a.astype('int')
            merged[sub_element] = list(a)

    return (merged)

def variable_sorter(dataframe_path, the_latest_var):
    data, sheet_names = data_read(dataframe_path)
    data, sheet_names, weight_names = weight_resetter(data, sheet_names)
    var_names = ['카테고리', '사이즈', '업체', '컬러']
    data, sheet_names, heads = stripper(data, sheet_names, var_names)
    new_data = sheet_mergers(data, sheet_names, heads, var_names)
    variables = pd.read_excel(the_latest_var, index_col=0)
    final_data = pd.DataFrame()

    final_data['이미지경로'] = new_data['이미지경로']
    new_data = new_data.reset_index()
    variables = list(variables.iloc[:, 0])

    for v in variables:
        try:
            final_data[v] = new_data[v].astype('int')
        except:
            final_data[v] = int(0)

    return (final_data)


def load_latest_model():
    print("Start loading model...")
    source = "model_weights"
    weights_list = list(glob.glob(os.path.join(source, '*.h5')))
    weight_file = max(weights_list, key=os.path.getctime)

    models_list = list(glob.glob(os.path.join(source, '*.json')))
    model_file = max(models_list, key=os.path.getctime)

    model_json_file = open(model_file, 'r')
    loaded_model_from_json = model_json_file.read()
    model_json_file.close()
    loaded_model = model_from_json(loaded_model_from_json)

    loaded_model.load_weights(weight_file)
    print("model loaded.")
    return (loaded_model)


def img_downloader(new_data):
    print("Downloading Images...")
    for i in tqdm(new_data.index, desc="Downloading Image Files:"):
        try:
            name = file_format_parser(new_data, i)
            print(name, "\n");
            # make_path("pred_temp_img")
            new_name = "pred_temp_img/" + str(i) + name
            # request.urlretrieve(new_data["이미지경로"][i], new_name)
            file_writer(new_name, new_data, i)
            # request.URLopener.retrieve(new_data["이미지경로"][i], new_name)
            # print("image: " + str(i) + " - " + new_name)
        except Exception as e:
            print(e)
            pass

# Checking file formats
def file_format_parser(dataset, index):
    #print(dataset['이미지경로'][index],"\n")
    """
    f = dataset['이미지경로'][index].find(".jpg", 20)
    if f == -1 :
        f = dataset['이미지경로'][index].find(".png", 20)
    if f == -1 :
        f = dataset['이미지경로'][index].find(".gif", 20)
    if f == -1 :
        f = dataset['이미지경로'][index].find(".jpeg", 20)
    if f == -1 :    
        f = dataset['이미지경로'][index].find(".JPG", 20)
    if f == -1 :
        f = dataset['이미지경로'][index].find(".PNG", 20)
    if f == -1 :
        f = dataset['이미지경로'][index].find(".GIF", 20)   
    #f1 = dataset['이미지경로'][index].find("?", 40)
    f1 = dataset['이미지경로'][index].find(".", 40)
    if f1 == -1 :
        #f1 = len(str(dataset['이미지경로'][index]))
        return ".jpeg"
    last = len(dataset['이미지경로'][index])
    name = str(dataset['이미지경로'][index])[f:last]
    """
    return ".jpeg"

# Assuring images be JPG format   
def separating_jpg(new_data, source, dest):
    idx = []
    new_data['cleaned'] = 0
    for i in tqdm(new_data.index, desc="Cleaning Image files:"):
        name = dest + str(i) + ".jpg"
        try:
            
            file = source + "/" + str(i) + file_format_parser(new_data, i)
            img = Image.open(file)
            img = img.convert('RGB')
            
            make_path(name)
            img.save(name, "jpeg", quality = 100)
            
            new_data['cleaned'][i] = int(1)
            idx.append(i)
        except:
            print("error exists: " + name)
            new_data['cleaned'][i] = int(0)
    return idx        
        
# Assuring images be JPG format   
def separating_jpg_predict(new_data, source, dest, data):
    new_data['cleaned'] = 0
    for i in tqdm(new_data.index, desc="Cleaning Image files:"):
        name = dest + str(i) + ".jpg"
        try:
            file = source + "/" + str(i) + file_format_parser(new_data, i)
            img = Image.open(file)
            img = img.convert('RGB')

            make_path(name)
            img.save(name, "jpeg", quality = 100)

            new_data['cleaned'][i] = int(1)
        except:
            print("======================================= error ", i ," ============================================")
            data = data.drop(i, axis=0)
            print("error exists: " + name)
            new_data['cleaned'][i] = int(0)
    return data        

def file_writer(path_name, dataset, index):
    try:
        with open(path_name, 'wb') as f:
            f.write(requests.get(dataset["이미지경로"][index], timeout=3).content)
                
    except:
        with open(os.getcwd()+"/error.txt", mode="a") as log:
            log.write(dataset["이미지경로"][index]+"\n")# -*- coding: utf-8 -*-
            

seed_number = 42

random.seed(seed_number)

def image_loader(data_matrix, sourcepath):
    img = []
    print(data_matrix.index)
    for i in data_matrix.index:
        str_i = sourcepath + "/" + str(i) + ".jpg"
        #print("loading a image")
        with open("log/log.txt", "w") as log:
            if os.path.exists(str_i) and os.path.getsize(str_i) > 0:
                image = cv2.imread(str_i)
    #            print("===========image0000: ",image)
                image= cv2.resize(image, (IMAGE_DIMS[1], IMAGE_DIMS[0]))
                image = img_to_array(image)
    #            print("===========image: ",image)
    #            img.append(str(i) + ".jpg")
                img.append(image)
    #            print("===========img: ",img)
            else:
                log.write(str_i+"\n")
                print(str_i)
    print("transforming images...")
    img = np.array(img, dtype = "float") / 255.0
    
    return(img)

def dataset_loader(datafile, sourcepath, var_names, sheet_names):
    train_X_img = ""
    train_X = ""
    train_Y = ""
    test_X_img = ""
    test_X = ""
    test_Y = ""
    try :
        print("start loading dataset")
        
        print("retrieving data matrix")
        data = pd.read_excel(datafile)
        data = data[data['cleaned'] == 1]
        var_names = pd.read_excel(var_names)
        var_names = list(var_names['variable_names'])
        sheet_names = pd.read_excel(sheet_names)
        sheet_names = list(sheet_names['sheet_names'])
        
        #$data = data[(data['매니시(0902)'] == 1) | (data['러블리상(0930)'] == 1) | (data['러블리중(0904)'] == 1) | (data['유니크(0908)'] == 1) | (data['섹시(0910)'] == 1)]
        #data = data.drop([7975]) # an observation with problem
    
        Y = pd.DataFrame()
        print("====Y====\n", Y)
#        print(len(data))
        print(sheet_names)
        for s in sheet_names:
            print("====s:",s,"\n")
            Y[str(s)] = data[str(s)]
            
    
        data_X = pd.DataFrame()
        for v in var_names:
            data_X[v] = data[v]
        del(data)
        
        for s in sheet_names:
             Y[str(s)] = Y[str(s)] >= 1
             Y[str(s)] = Y[str(s)].astype('int')
    

        print("Splitting data into training set and test set")
        train_X, test_X, train_Y, test_Y = train_test_split(data_X, Y, random_state = seed_number, train_size = 0.75)
        del(data_X)
        del(Y)
        print("start loading image data")
        train_X_img = image_loader(train_X, sourcepath)
        test_X_img = image_loader(test_X, sourcepath)
    except Exception as e :
        print("===error: ", e,"\n")
        raise
    
    return(train_X_img, train_X, train_Y, test_X_img, test_X, test_Y)
  
"""    
def get_evaluation(test_Y, y_pred):
    
    dat = pd.DataFrame()
    dat['variable_name'] = list(test_Y.columns)
    accu = []
    rec = []
    prec = []
    
    #class-wise metrics
    for i in range(n_classes):
        accu.append(accuracy_score(test_Y.iloc[:,i], y_pred[:,i]))
        rec.append(recall_score(test_Y.iloc[:,i], y_pred[:,i]))
        prec.append(precision_score(test_Y.iloc[:,i], y_pred[:,i]))
        
    dat["accuracy"] = accu
    dat["Recall"] = rec
    dat["Precision"] = prec
    
    return(dat)
"""  

def get_latest_dataset(dataset_dir):

    dataset_files = list(glob.glob(os.path.join(dataset_dir, '*.xlsx')))
#    modified_data = [os.path.getmtime(f) for f in dataset_files]
#    date = zip(dataset_files, modified_data)
#    date = sorted(date, key = lambda x: x[-1])
#    most_recent = date[0][0]
    latest_file = max(dataset_files, key=os.path.getctime)
    
    f_name_l = latest_file.find(".xlsx")
    name = ""
    for i in range(26, f_name_l):
        a = latest_file[i]
        name = name + a

    sourcepath = "cleaned_images/" + name
    datafile = "data_cleaned/data_cleaned_" + name + ".xlsx"
    var_names = "Variable_Names/varnames_" + name + ".xlsx"
    sheet_names = "Sheet_Names/sheetnames_" + name + ".xlsx"
    return(sourcepath, datafile, name, var_names, sheet_names)
    
def auc_roc(y_true, y_pred):
    # any tensorflow metric
    value, update_op = tf.contrib.metrics.streaming_auc(y_pred, y_true)

    # find all variables created for this metric
    metric_vars = [i for i in tf.local_variables() if 'auc_roc' in i.name.split('/')[1]]

    # Add metric variables to GLOBAL_VARIABLES collection.
    # They will be initialized for new session.
    for v in metric_vars:
        tf.add_to_collection(tf.GraphKeys.GLOBAL_VARIABLES, v)

    # force to update metric values
    with tf.control_dependencies([update_op]):
        value = tf.identity(value)
        return value
         
IMAGE_DIMS = (128, 128, 3)

