# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 11:02:27 2018

@author: Josh
"""

import sys
sys.path.append("/home/otdeal/다운로드/Project_Other_Publish")

from tools import *#make_path, data_read, weight_resetter, stripper, file_format_parser, separating_jpg_predict, file_writer

import pandas as pd
import numpy as np
import os
import glob
import errno
import shutil
import time
from datetime import datetime
from urllib import request
import imghdr
from PIL import Image
#import wget
#from tqdm import tqdm
from time import sleep 
import random
import urllib
from io import BytesIO
import requests

import wide_deep_elu
import predict

"""
## Writing permission on file
def make_path(f_name):
    if not os.path.exists(os.path.dirname(f_name)):
        try:
            os.makedirs(os.path.dirname(f_name))
        except OSError as exc:
            if exc.errno != errno.EXXIST:
                raise
                
                
# loading data into dict_dataframe
def data_read(data):
    
    df = pd.read_excel(data, sheet_name = None)
    sheet_names = list(df.keys())
#    print("======df======"+"\n");
#    print(df);
#    print("\n");
#    print("======df======"+"\n");
#    print("======sheet_names======"+"\n");
#    print(sheet_names);
#    print("\n");
#    print("======sheet_names======"+"\n");
    
    return(df, sheet_names)
    

## Sheet names to variables
def weight_resetter(data, sheet_names):
    
    weight_names = []
    
    for sheet in sheet_names:
        
        data[sheet][sheet] = int(1)
        data[sheet].rename(columns = {'가중치': sheet + '_가중치'}, inplace = True)
        weight_names.append(sheet)
        weight_names.append(sheet + '_가중치')
        
    return(data, sheet_names, weight_names)
    

## parsing element and making dummy variables
def stripper(data, sheet_names, var_names):
    
    v = {}
    
    for var_name in var_names:
        v[var_name] = pd.Series()
    
    for sheet in sheet_names:

        for var_name in var_names:
            t = data[sheet][var_name].str.split(',').apply(pd.Series, 1).stack()
            v[var_name] = pd.concat([v[var_name], t], axis = 0) 
            t = pd.get_dummies(t)
            t = t.sum(level = 0)
            data[sheet] = pd.concat([data[sheet], t], axis = 1)
            
    for var_name in var_names:
        v[var_name] = sorted(list(pd.unique(v[var_name])))
            
    return(data, sheet_names, v)
    
def sheet_merger(data, sheet_names, heads, weight_names):
    
    initial_sheet_names = sheet_names[0]
    temp = list(sheet_names)
    del(temp[0])
    
    dataFrame = data[initial_sheet_names]
    
    for t in temp:        
        dataFrame = dataFrame.append(data[t], ignore_index = True, sort = False)
        
    dataFrame = dataFrame.groupby(by=['이미지경로']).sum()
    merged = pd.DataFrame()
    
    merged['이미지경로'] = dataFrame.index
    
    for w in weight_names:
        merged[w] = list(dataFrame[w])
        
    for v in var_names:
        for sub_element in heads[v]:
            a = dataFrame[sub_element] >= 1
            a = a.astype('int')
            merged[sub_element] = list(a)
    
    return(merged, sheet_names)
    

# Checking file formats
def file_format_parser(dataset, index):
    print(dataset['이미지경로'][index],"\n")
    f = dataset['이미지경로'][index].find(".jpg", 20)
    if f == -1 :
        f = dataset['이미지경로'][index].find(".png", 20)
    if f == -1 :
        f = dataset['이미지경로'][index].find(".gif", 20)
    if f == -1 :
        f = dataset['이미지경로'][index].find(".jpeg", 20)
    if f == -1 :    
        f = dataset['이미지경로'][index].find(".JPG", 20)
    if f == -1 :
        f = dataset['이미지경로'][index].find(".PNG", 20)
    if f == -1 :
        f = dataset['이미지경로'][index].find(".GIF", 20)   
    #f1 = dataset['이미지경로'][index].find("?", 40)
    f1 = dataset['이미지경로'][index].find(".", 40)
    if f1 == -1 :
        #f1 = len(str(dataset['이미지경로'][index]))
        return ".jpg"
    last = len(dataset['이미지경로'][index])
    name = str(dataset['이미지경로'][index])[f:last]

    return name    

# Assuring images be JPG format   
def separating_jpg(new_data, source, dest):
    idx = []
    new_data['cleaned'] = 0
    for i in tqdm(new_data.index, desc="Cleaning Image files:"):
        name = dest + str(i) + ".jpg"
        print(name,"\n");
        try:
            file = source + "/" + str(i) + file_format_parser(new_data, i)
            img = Image.open(file)
            img = img.convert('RGB')

            make_path(name)
            img.save(name, "jpeg", quality = 100)

            new_data['cleaned'][i] = int(1)
            idx.append(i)
        except:
            print("error exists: " + name)
            new_data['cleaned'][i] = int(0)
    return idx        
        
# Assuring images be JPG format   
def separating_jpg_predict(new_data, source, dest, data):
    new_data['cleaned'] = 0
    for i in tqdm(new_data.index, desc="Cleaning Image files:"):
        name = dest + str(i) + ".jpg"
        print(name,"\n");
        try:
            file = source + "/" + str(i) + file_format_parser(new_data, i)
            img = Image.open(file)
            img = img.convert('RGB')

            make_path(name)
            img.save(name, "jpeg", quality = 100)

            new_data['cleaned'][i] = int(1)
        except:
            print("======================================= error ", i ," ============================================")
            data = data.drop(i, axis=0)
            print("error exists: " + name)
            new_data['cleaned'][i] = int(0)
    return data        

def file_writer(path_name, dataset, index):
    try:
        with open(path_name, 'wb') as f:
            f.write(requests.get(dataset["이미지경로"][index], timeout=3).content)
                
    except:
        with open(os.getcwd()+"/error.txt", mode="a") as log:
            log.write(dataset["이미지경로"][index]+"\n")
 
"""       
#%%  
if __name__ == '__main__':
    """
    dataset_folder = "data" ## Path in dataset list
    data_files = list(glob.glob(os.path.join(dataset_folder, '*.xlsx')))
    the_latest = min(data_files, key=os.path.getctime)
    data, sheet_names = data_read(the_latest) ## loading data into dict_dataframe

    data, sheet_names, weight_names = weight_resetter(data, sheet_names) ## Sheet names to variables
    
    ## parsing element and making dummy variables
    var_names = ['카테고리', '사이즈', '업체','컬러']
    data, sheet_names, heads = stripper(data, sheet_names, var_names)
    
    ## Merging sheets into one dataframe
    new_data, sheet_names = sheet_merger(data, sheet_names, heads, weight_names)
    
    try:
        for i in tqdm(new_data.index, desc = "Downloading Images Files:"):
    #        file_format = new_data['이미지경로'][i].find(".", 40)
    #        name = ""
    #        for j in range(file_format, len(str(new_data['이미지경로'][i]))):
    #            a = str(new_data['이미지경로'][i][j])
    #            name = name + a
    #        name = str(i) + name
            
            name = file_format_parser(new_data, i)
            
            path_name = "dataset/" + str(i) + name
         
            #make_path(path_name)
            #request.urlretrieve(new_data["이미지경로"][i], path_name)
            #print("\n", new_data['이미지경로'][i])
            #response = request.urlopen(new_data["이미지경로"][i])
            #with open(path_name, 'wb') as f:
            #    f.write(requests.get(new_data["이미지경로"][i]).content)
            file_writer(path_name, new_data, i)
    except Exception as e:
        print("==========for error=========\n",e)
        exit()
    
    
    now = datetime.now().strftime('%Y%m%d_%H%M%S')
    
    source = "dataset"
    destpath = "cleaned_images/"+ now +"/"
    
    print("re-processing images...")        
    separating_jpg(new_data, source, destpath)
              
    ## export to excel file
    print("saving cleaned datafile...")
    if (sum(new_data.duplicated(['이미지경로'])) == int(0)): # Assuring there exists no duplication in the data.
        
        f_name = "data_cleaned/data_cleaned_" + now + ".xlsx"
        make_path(f_name)
        writer = pd.ExcelWriter(f_name)
        new_data.to_excel(writer, encoding="utf8")
        writer.save()
     
    else:
        print("Data Cleaning failed because there exist " 
              + sum(new_data.duplicated(['이미지경로'])) 
              + "duplications in final dataframe.")
    
    print("Saving variable names...")
    var_names = ['카테고리', '사이즈', '컬러']    
    
    var_to_export = []
    for var in var_names:
        for v in heads[var]:
            var_to_export.append(v)
            
            
    var_to = pd.DataFrame()
    var_to['variable_names'] = var_to_export
    var_file_name = "Variable_Names/varnames_" + now + ".xlsx"
    make_path(var_file_name)
    var_to.to_excel(var_file_name)
    
    sheet_to = pd.DataFrame()
    sheet_to['sheet_names'] = list(data.keys())
    sheet_file_name = "Sheet_Names/sheetnames_" + now + ".xlsx"
    make_path(sheet_file_name)
    sheet_to.to_excel(sheet_file_name)
    print("Cleaning process is complete.\n\n")
    


    print("------start deep learning--------")
    wide_deep_elu.alphago_start()
    """
    print("-----------start preict----------")
    predict.alphago_predict()
    


  
    
    
