#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 11:06:10 2018

@author: josh
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 13:42:56 2018

@author: Josh
"""

import pandas as pd
import numpy as np
import cv2
from keras.models import Sequential, Model
from keras.layers import ELU # Loss layer
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers.core import Activation, Flatten, Dropout, Dense
from keras.preprocessing.image import img_to_array, ImageDataGenerator
from keras.layers.merge import Concatenate
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, TensorBoard
from keras import regularizers
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, recall_score
import glob
import os
import errno
from data_cleaning import make_path
import tensorflow as tf
from numpy.random import seed
seed_number = 42
seed(seed_number)

def image_loader(data_matrix, sourcepath):
    img = []   
    
    ## Loading images from correspoding indices of data matrix.
    for i in data_matrix.index:
        str_i = sourcepath + "/" + str(i) + ".jpg"
        print("loading a image")
        print(str_i)
        image = cv2.imread(str_i)
        image= cv2.resize(image, (IMAGE_DIMS[1], IMAGE_DIMS[0]))
        image = img_to_array(image)
        img.append(image)
        
    print("transforming images...")
    img = np.array(img, dtype = "float") / 255.0
    
    return(img)
    

def dataset_loader(datafile, sourcepath, var_names, sheet_names):
    print(datafile)
    print(sourcepath)
    print(var_names)
    print(sheet_names)
    print("start loading dataset")
    
    print("retrieving data matrix")
    data = pd.read_excel(datafile)
    data = data[data['cleaned'] == 1]
    var_names = pd.read_excel(var_names)
    var_names = list(var_names['variable_names'])
    sheet_names = pd.read_excel(sheet_names)
    sheet_names = list(sheet_names['sheet_names'])
    
    #$data = data[(data['매니시(0902)'] == 1) | (data['러블리상(0930)'] == 1) | (data['러블리중(0904)'] == 1) | (data['유니크(0908)'] == 1) | (data['섹시(0910)'] == 1)]
    #data = data.drop([7975]) # an observation with problem

    Y = pd.DataFrame()
    for s in sheet_names:
        Y[s] = data[s]
        

    data_X = pd.DataFrame()
    for v in var_names:
        data_X[v] = data[v]
    del(data)
    
    for s in sheet_names:
         Y[s] = Y[s] >= 1
         Y[s] = Y[s].astype('int')


    print("Splitting data into training set and test set")
    train_X, test_X, train_Y, test_Y = train_test_split(data_X, Y, random_state = seed_number, train_size = 0.75)
    del(data_X)
    del(Y)
    
    print("start loading image data")
    train_X_img = image_loader(train_X, sourcepath)
    test_X_img = image_loader(test_X, sourcepath)
    
    return(train_X_img, train_X, train_Y, test_X_img, test_X, test_Y)
  
    
def get_evaluation(test_Y, y_pred):
    
    dat = pd.DataFrame()
    dat['variable_name'] = list(test_Y.columns)
    accu = []
    rec = []
    prec = []
    
    #class-wise metrics
    for i in range(n_classes):
        accu.append(accuracy_score(test_Y.iloc[:,i], y_pred[:,i]))
        rec.append(recall_score(test_Y.iloc[:,i], y_pred[:,i]))
        prec.append(precision_score(test_Y.iloc[:,i], y_pred[:,i]))
        
    dat["accuracy"] = accu
    dat["Recall"] = rec
    dat["Precision"] = prec
    
    return(dat)
     

def get_latest_dataset(dataset_dir):

    dataset_files = list(glob.glob(os.path.join(dataset_dir, '*.xlsx')))
#    modified_data = [os.path.getmtime(f) for f in dataset_files]
#    date = zip(dataset_files, modified_data)
#    date = sorted(date, key = lambda x: x[-1])
#    most_recent = date[0][0]
    latest_file = max(dataset_files, key=os.path.getctime)
    
    f_name_l = latest_file.find(".xlsx")
    name = ""
    for i in range(26, f_name_l):
        a = latest_file[i]
        name = name + a

    sourcepath = "cleaned_images/" + name
    datafile = "data_cleaned/data_cleaned_" + name + ".xlsx"
    var_names = "Variable_Names/varnames_" + name + ".xlsx"
    sheet_names = "Sheet_Names/sheetnames_" + name + ".xlsx"
    return(sourcepath, datafile, name, var_names, sheet_names)
    
def auc_roc(y_true, y_pred):
    # any tensorflow metric
    value, update_op = tf.contrib.metrics.streaming_auc(y_pred, y_true)

    # find all variables created for this metric
    metric_vars = [i for i in tf.local_variables() if 'auc_roc' in i.name.split('/')[1]]

    # Add metric variables to GLOBAL_VARIABLES collection.
    # They will be initialized for new session.
    for v in metric_vars:
        tf.add_to_collection(tf.GraphKeys.GLOBAL_VARIABLES, v)

    # force to update metric values
    with tf.control_dependencies([update_op]):
        value = tf.identity(value)
        return value
         
IMAGE_DIMS = (128, 128, 3)
    

if __name__ == '__main__':
     dataset_dir = "data_cleaned" 
     sourcepath, datafile, name, var_names, sheet_names = get_latest_dataset(dataset_dir)
         
         
     #IMAGE_DIMS = (224, 224, 3)

     train_X_img, train_X, train_Y, test_X_img, test_X, test_Y = dataset_loader(datafile, 
                                                                                sourcepath, 
                                                                                var_names, 
                                                                                sheet_names)
     
     batch_size = 12
     EPOCHS = 500
     n_classes = len(test_Y.columns)
     
     ## Deep Model
     (height, width, depth) = (IMAGE_DIMS[1], IMAGE_DIMS[0], IMAGE_DIMS[2])
     inputShape = (height, width, depth)
     chanDim = -1
     Deep_model = Sequential()
     # CONV => RELU => POOL
     Deep_model.add(Conv2D(32, (3, 3), padding="same", input_shape=inputShape))
     Deep_model.add(ELU(alpha = 1.0))
     Deep_model.add(BatchNormalization(axis=chanDim, epsilon=0.001))
     Deep_model.add(MaxPooling2D(pool_size=(3, 3)))
     Deep_model.add(Dropout(0.25))
     # (CONV => RELU) * 2 => POOL
     Deep_model.add(Conv2D(64, (3, 3), padding="same",input_shape=inputShape))
     Deep_model.add(ELU(alpha = 1.0))
     Deep_model.add(BatchNormalization(axis=chanDim, epsilon=0.001))
     Deep_model.add(Conv2D(64, (3, 3), padding="same",input_shape=inputShape))
     Deep_model.add(ELU(alpha = 1.0))
     Deep_model.add(BatchNormalization(axis=chanDim))
     Deep_model.add(MaxPooling2D(pool_size=(2, 2)))
     Deep_model.add(Dropout(0.25))
     # (CONV => RELU) * 2 => POOL
     Deep_model.add(Conv2D(128, (3, 3), padding="same",input_shape=inputShape))
     Deep_model.add(ELU(alpha = 1.0))
     Deep_model.add(BatchNormalization(axis=chanDim, epsilon=0.001))
     Deep_model.add(Conv2D(128, (3, 3), padding="same",input_shape=inputShape))
     Deep_model.add(ELU(alpha = 1.0))
     Deep_model.add(BatchNormalization(axis=chanDim, epsilon=0.001))
     Deep_model.add(MaxPooling2D(pool_size=(2, 2)))
     Deep_model.add(Dropout(0.25))
     # first set of FC
     Deep_model.add(Flatten())
     Deep_model.add(Dense(1024))
     Deep_model.add(ELU(alpha = 1.0))
     Deep_model.add(BatchNormalization(epsilon=0.001))
     Deep_model.add(Dropout(0.5))
     
     ## Wide Modeladam', loss = 'binary_crossentropy', metrics = ['accuracy'])
     Wide_model = Sequential()
     Wide_model.add(Dense(train_X.shape[1], input_shape = (train_X.shape[1], )))
     Wide_model.add(Activation("relu"))
     Wide_model.add(BatchNormalization())
     
     ## Wide and Deep Model (Concatenating Layers)
     concatOut = Concatenate()([Deep_model.output, Wide_model.output])
     Wide_and_Deep = Dense(n_classes, activation = 'sigmoid')(concatOut)
     #Wide_and_Deep = Dense(n_classes, activation = 'softmax')(concatOut)
     Wide_and_Deep = Model([Deep_model.input, Wide_model.input], outputs=Wide_and_Deep)
     opt = Adam(lr = 1e-3, decay = 1e-3 / EPOCHS)
     Wide_and_Deep.compile(loss = 'binary_crossentropy', optimizer = opt, metrics=['accuracy', auc_roc])
     #Wide_and_Deep.compile(loss = 'categorical_crossentropy', optimizer = opt, metrics=['accuracy', auc_roc])
     
     
     ## Callbacks
     tb_hist = TensorBoard(log_dir='./graph', histogram_freq=0, write_graph=True, write_images=True)
     earlyStopping = EarlyStopping(monitor = 'val_loss', patience = 30, verbose = 1, mode = 'auto')
     reduce_lr = ReduceLROnPlateau(monitor = 'val_loss', factor = 0.2, patience = 5, min_lr = 0.001)
     mcp_file = "model_weights/model_weight_checkpoint_" + name + ".hdf5"
     make_path(mcp_file)
     mcp_save = ModelCheckpoint(mcp_file, save_best_only=True, monitor='val_loss', mode='auto')
     
     
     generator = ImageDataGenerator(horizontal_flip = True, vertical_flip = True, width_shift_range = 0.1, height_shift_range = 0.1, zoom_range = 0.1, rotation_range = 15)
     
     def augmentor(IMG, X, Y):
          gen_1 = generator.flow(IMG, Y, batch_size = batch_size, seed = seed_number)
          gen_2 = generator.flow(IMG, X, batch_size = batch_size, seed = seed_number)
          while True:
               X1_ = gen_1.next()
               X2_ = gen_2.next()
               yield [X1_[0], X2_[1]], X1_[1]
     gen_flow = augmentor(train_X_img, train_X, train_Y)
     ## Fitting Wide and Deep Model
     #Wide_and_Deep.fit([train_X, train_X_img], train_Y, validation_data = ([test_X, test_X_img], test_Y), 
     #                  epochs = EPOCHS, batch_size = 12, shuffle = True, 
     #                  callbacks=[tb_hist, earlyStopping, reduce_lr, mcp_save])
     Wide_and_Deep.fit_generator(gen_flow, validation_data = ([test_X_img, test_X], test_Y), steps_per_epoch=len(train_X) / batch_size, epochs=EPOCHS, callbacks=[tb_hist, earlyStopping, reduce_lr, mcp_save])
     
     
     
     y_pred = Wide_and_Deep.predict([test_X_img, test_X])
     
     y_pred[y_pred >= 0.33] = 1
     y_pred[y_pred < 0.33] = 0
     
     ## Score
     
     scores = Wide_and_Deep.evaluate([test_X_img, test_X], test_Y)
     
     # saving model and weights
     j_file_name = "model_weights/model_json_" + name + ".json"
     make_path(j_file_name)
     model_json = Wide_and_Deep.to_json()
     with open(j_file_name, "w") as json_file:
         json_file.write(model_json)
     # serialize weights to HDF5
     w_file_name = "model_weights/model_weight_" + name + ".h5"
     Wide_and_Deep.save_weights(w_file_name)
     print("Saved model to disk")
     
     
     
     evaluation = get_evaluation(test_Y, y_pred)
     eval_name = "evaluation/evalutation_" + name +".csv"
     make_path(eval_name)
     evaluation.to_csv(eval_name)
     
     
