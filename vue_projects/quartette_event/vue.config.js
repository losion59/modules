const path = require('path');

module.exports = {
  publicPath: "",
  indexPath: "./quartette_event.html",

  //plugin options
  pluginOptions: {
  	'html': {
  		template: path.resolve(__dirname, "./public/source.html")
  	},
  },

  //dynamic webpackConfig
  chainWebpack: config =>{
  	if(config.plugins.has("copy")){
  		config.plugins.delete("copy");
  	}
  }
}
