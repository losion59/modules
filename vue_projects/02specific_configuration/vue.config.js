const path = require('path');
const index_name = "./helmet.html";

module.exports = {
  publicPath: "./",
  indexPath: index_name,

  css : {
  	extract: true,	
  	sourceMap: true,
  },

  pluginOptions: {
  	'html': {
  		template: path.resolve(__dirname, "./public/bone.html"),
  		filename: index_name

  	}
  },

  chainWebpack: config => {
   	if (config.plugins.has('copy')) {
      config.plugins.delete('copy')
    }
  }
  
}
