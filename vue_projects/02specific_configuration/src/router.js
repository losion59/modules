import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

const path = require('path');

Vue.use(Router)
export default new Router({
  mode: 'history',
  base: path.resolve(__dirname, ""),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
