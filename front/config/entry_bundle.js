//PROP
const env = require('./env_property');

module.exports={
    "head" : [env.VIEWS("head","js")],

    "main" : [env.VIEWS("main","js")],
    "curation" : [env.VIEWS("curation","js")],
    "alfred" : [env.VIEWS("alfred","js")]
}