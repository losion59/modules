//css
import "@/css/animation.scss"
import "@/css/font_bundle.scss"
import "./alfred.scss"
import "@/views/head/head.scss"
//third-party
import Vue from "@/js/vue"
import axios from "@/js/axios"
import Velocity from "velocity-animate"
import "@/js/jquery"
//class
import ArgResolver from "@class/resolver/ArgumentResolver"
//component
import ability from "@/js/components/ability/ability"
//polyfill
import "@babel/polyfill"


window.main_app = new Vue({
    
    el: "#app",

    components: {
        item: ()=>import("@/js/components/item/item"),
        ability
    },

    prop:["style_list"],

    data: {
        kinds: ["top","bottom","outer"],

        style: "Stylish",
        
        ai_msg: {
            title : "당신과 비슷한 패셔니스타는 박해진이네요",
            specific: `늘 어떤 옷을 입을지 즐기는 장신은 남들이 부러워할 패셔니스타시군요~<br>
                       친구들이 따라입거나 어디서 샀냐고 자주 물어보지 않으시나요?<br>
                       양말까지 센스입게 코디하시는 당신에게는 트렌디한 옷을 추천할게요.`,
            
            more: true,
            offset: 0,

            output: [90,100,90,100,90],
            kewords: ["자기만족형","편안함","코디형","유행","컬러형"]
        },

        curation_list: "",

    },
    
    computed: {
        background(){
            return `/resources/pc/img/bg${Math.floor(Math.random()*6)+1}.jpg`
        }
    },

    watch: {
        
    },

    methods: {
        cap(string){
            return string.charAt(0).toUpperCase() + string.slice(1)
        },
        to_url(string){
            return `url('${string}')`
        },

        filtered_list(kind){
            window.list = this.curation_list
            return this.curation_list[kind]
        },

        fetch_curation_list(style_list){

            // return axios.request({
            //                 method: "get",
            //                 url: "/rest/male/item",
            //                 param: {
            //                     style_list : ArgResolver.to_list(style_list)
            //                 }
            //             }).then(res=>{
            //                 this.curation_list = res.data
            //             })
        },

        //hyper
        to(address){
            location.href=address
        },

        //animation
        enter(el){
            el.style.opacity = 0
            Velocity(el,{ "max-height": "1000px", opacity: 1 }, { duration: 2000 })
        },
        before_leave(el){
            el.style.display="flex"
            Velocity(el,{ "max-height": 0, opacity: 0 },{ duration: 1000 })
        },
        leave(el){
            el.style.display="flex"
            Velocity(el,{ "max-height": 0, opacity: 0 },{ duration: 1000 })
        }


    },
    
    mounted(){
        //await this.fetch_arriavl_list(this.style_list)
        //await this.fetch_curation_list(this.style_list)
        
        this.curation_list = Object.create({})

        this.kinds.forEach(kind=>{
            this.curation_list[kind] = new Array()

            for(let i=0;i<8;i++){
                this.curation_list[kind].push({
                    url: "http://mblogthumb1.phinf.naver.net/MjAxODAzMTFfMTkg/MDAxNTIwNzMxNjM4MjQ2.TH3pMELNPse1-qI9FHTsNywaj27NmoGdzh72LyhytYUg.5BsQu3NsczolitzUay1aGXJt0eZ4Wk7TIZcRhgNq_E4g.JPEG.wellduo33/IMG_2588.jpg?type=w800",
                    sale_rate : "0",
                    new_rate: "1",
                    
                    style_main: "",
                    style_time: "빈하루",
                    product_name: "basic_rider jacket",
                    price: "23000"
                })
            }
        })
        
        this.ai_msg.offset = this.$refs.extra.offsetHeight
        this.ai_msg.more = !this.ai_msg.more

    }
    
})
