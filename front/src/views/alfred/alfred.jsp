<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="옷딜, 모던, 남성의류쇼핑몰 베스트 모아서 내 사이즈로 자동 추천받자 ">
<meta name="keywords" content="옷딜, 남성쇼핑몰, 쇼핑앱, 쇼핑어플, 20대남성쇼핑몰">
<meta name="author" content="옷딜">
<!--property for opengraph-->
<meta property="og:image" content="https://www.otdeal.io/resources/mobile/images/common/icn/logo2.png" />

<title>옷딜</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/pc/male/common/head.jsp" flush="true"/>
    <div id="app">
        
        <div id="stlye_back" 
             :style="{ backgroundImage:to_url(background) }">
            <div class="title">
                <span><span>#</span>Curation</span>
                <span>당신의 패션은 클래식 스타일</span>
            </div> 
            <div class="option">
                <div @click="ai_msg.more = !ai_msg.more">더보기</div><div id="more"></div>
                <div @click="to('/curation_temp')">재설정</div><div id="reset"></div>
            </div>
        </div>
        <div id="curated">
            <div id="portrait"></div>
            <div id="repo"> {{ style }} </div>
            <div id="subline"></div>
            <div class="title">
                <span>“</span>
                <span> {{ ai_msg['title'] }} </span>
                <span>”</span>
            </div>
            <transition v-on:enter="enter" 
                        v-on:leave="leave" v-on:before-leave="before_leave">
                <div id="extra_info" ref="extra"
                     v-if="ai_msg.more">
                    <div class="specific"
                        v-html="ai_msg['specific']">
                    </div>
                    <ability 
                             :xoffset="1000" :yoffset="500"
                             :phase="7" :asset="ai_msg.output" :keywords="ai_msg.kewords"></ability>
                </div>
            </transition>
        </div>

        <div class="curations_container"
             v-for="(kind,idx) in kinds" :key="kind" >
            <div class="title"> {{ cap(kind) }} </div>
            <div class="curations">
                <item class="item"
                      v-for="(curation,idx) in filtered_list(kind)" :key="idx"
                      :item="curation">
                    <template #like>
                        <div class="like"></div>
                    </template>
                </item>
            </div>
        </div>

    </div>

</body>

</html>