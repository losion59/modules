<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="옷딜, 모던, 남성의류쇼핑몰 베스트 모아서 내 사이즈로 자동 추천받자 ">
<meta name="keywords" content="옷딜, 남성쇼핑몰, 쇼핑앱, 쇼핑어플, 20대남성쇼핑몰">
<meta name="author" content="옷딜">
<!--property for opengraph-->
<meta property="og:image" content="https://www.otdeal.io/resources/mobile/images/common/icn/logo2.png" />
<title>옷딜</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/pc/male/common/head.jsp" flush="true"/>
    <div id="app">
        <div id="ad_container">
           <img src="./static/main_img.jpg"> 
        </div>
        
        <div id="arriavals_container">
            <div class="title"> NEW ARRIVALS </div>
            <div class="selection">
                <div class="style" :class="{active:all_blue}"
                     @click="fetch_arrival_list(Object.keys(style_list))"> ALL </div>
                <div class="style" :class="{active: prop['is_activated']}"
                     v-for="(prop,style) in style_list"
                     @click="fetch_arrival_list(style)">
                    {{ style }}
                </div>
            </div>
            <div id="arrivals">
                <transition name="slide-list">
                    <div class="arrival"
                        :key="'ALL'"
                        v-if="current_style == 'ALL'">
                        <item class="item"
                        v-for="(arrival,idx) in arrival_list" :key="arrival.url+idx"
                        :item="arrival" :sale="true"></item> 
                    </div>
                    <div class="arrival"
                        v-for="(prop,style) in style_list" :key="style"
                        v-if="current_style == style">
                        <item class="item"
                            v-for="(arrival,idx) in arrival_list" :key="arrival.url+idx"
                            :item="arrival" :sale="true"></item> 
                    </div>
                </transition>
            </div>
        </div>
        <div id="curations_container">
            <div class="title" v-if="curation_list.length != 0">Curation for You</div>
            <transition-group name="fill-list" tag="div"
                              id="curations"
                              appear>
                <item class="item"
                      v-for="(curation,idx) in curation_list" :key="idx"
                      :item="curation">
                    <template #like>
                        <div class="like"></div>
                    </template>
                </item>
            </transition-group>
        </div>
        <!-- <div id="more"
             @click="fetch_curation_list()">
            <span>more...</span>
        </div> -->

        <div id="curations_container">
            <div class="title" v-if="curation_list.length == 0">Sale Products</div>
            <transition-group name="fill-list" tag="div" id="curations" appear>
                <item class="item"
                        v-for="(saleproduct,idx) in saleproduct_list" :key="idx"
                        :item="saleproduct">
                </item>
            </transition-group>
        </div>
    </div>

<script type="text/javascript" src="/resources/common/js/common.js"></script>
</body>
</html>