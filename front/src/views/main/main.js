//css
import "@/css/animation.scss"
import "@/css/font_bundle.scss"
import "./main.scss"
import "@/views/head/head.scss"
//third-party
import Vue from "@/js/vue"
import axios from "@/js/axios"
import "@/js/jquery"

import ArgResolver from "@class/resolver/ArgumentResolver"

import "@babel/polyfill"

window.main_app = new Vue({
    
    el: "#app",

    components: {
        item: ()=>import("./components/item"),
    },

    prop:["style_list"],

    data: {
        all_blue: true,
        style_list: {
            'Casual': {
                is_activated: false
            },
            'Modern':{
                is_activated: false
            }
        },
        current_style: "ALL",

        arrival_list: [],
        curation_list: [],
        saleproduct_list: []
    },

    created() {
        
    },
    
    computed: {
        
    },

    watch: {
        all_blue(value){
            if(value){
                for(let [style,prop] of Object.entries(this.style_list)){
                    prop.is_activated = false
                }
            }
            this.curren_style = "ALL"
            return value
        },

        arrival_list(list){
            return ArgResolver.as_won_list(list)
        },
        curation_list(list){
            return ArgResolver.as_won_list(list)
        },
        saleproduct_list(list){
            return ArgResolver.as_won_list(list)
        },
    },

    methods: {

        fetch_arrival_list(style_list){
            console.log("======style_list: "+style_list);
            window.main_app.loadArrivalItem(style_list);
            
            style_list = ArgResolver.to_list(style_list)
            
           
            // //setTimeout(()=>this.arrival_list.splice(0,3),10000)
            // for(var i=0;i<3;i++){
            //     this.arrival_list.push({ 
            //         url:"https://t1.daumcdn.net/cfile/tistory/265FF83C58E46EDF19",
            //         sale_rate : "20",
            //         new_rate: "0",
                    
            //         style_main: "데일리",
            //         style_time: "퇴근 후 영화관",
            //         cody: "Casual",
            //         product_name: "인스벨 니트 조끼(4color)",
            //         price: "14000"
    
            //     })
            // }
            this.current_style = style_list[0]
            
            let count = 0
            this.all_blue = false
            for(let[style,prop] of Object.entries(this.style_list)){
                for(let s of style_list){
                    prop.is_activated=false
                    if(s==style){
                        prop.is_activated=true
                        count += 1
                    }
                }
            }
            if(count==Object.keys(this.style_list).length)this.all_blue=true

            /*
            return axios.request({
                            method: "get",
                            url: "/rest/male/item",
                            param: {
                                style_list : style_list
                            }
                        }).then(res=>{
                            let count = 0
                            this.all_blue = false
                            for(let[style,prop] of Object.entries(this.style_list)){
                                for(let s of style_list){
                                    prop.is_activated=false
                                    if(s==style){
                                        prop.is_activated=true
                                        count += 1
                                    }
                                }
                            }
                            if(count==Object.keys(this.style_list).length)this.all_blue=true
                            this.arrival_list = res.data
                        })
            */

        },
        fetch_curation_list(style_list){

            // for(let i=0;i<10;i++){
            //     this.curation_list.push({
            //         img_link: "http://mblogthumb1.phinf.naver.net/MjAxODAzMTFfMTkg/MDAxNTIwNzMxNjM4MjQ2.TH3pMELNPse1-qI9FHTsNywaj27NmoGdzh72LyhytYUg.5BsQu3NsczolitzUay1aGXJt0eZ4Wk7TIZcRhgNq_E4g.JPEG.wellduo33/IMG_2588.jpg?type=w800",
            //         sale_percent : "0",
            //         // new_rate: "1",
                    
            //         style_main: "",
            //         style_time: "빈하루",
            //         item_name: "basic_rider jacket",
            //         item_price: "23000"
            //     })
            // }

            // return axios.request({
            //                 method: "get",
            //                 url: "/rest/male/item",
            //                 param: {
            //                     style_list : ArgResolver.to_list(style_list)
            //                 }
            //             }).then(res=>{
            //                 this.curation_list = res.data
            //             })
        },

        loadSaleList() {
            if (this.curation_list.length > 0)
                return;

            console.log("#### ctreated!!!!");
            OtUtil.send({}, {
                url      : '/req/saleProduct/page/' +5,
                param   :  { testParam : "test!!!"},                
                verbose  : false,
                callback : {
                    success : function(data) {                                                     
                        window.main_app.saleproduct_list = data.res;
                        console.log("#### callback_________!!!");
                        console.log(window.main_app.saleproduct_list); 
    
                        for (var i=0; i < window.main_app.saleproduct_list.length; i++){
                            console.log(i + " : " + window.main_app.saleproduct_list[i].item_name);
                        }
                    }
                }
            });
        },

        loadArrivalItem(style_list) {
            var list = []
            // console.log("typeof: ", typeof style_list);
            if (typeof style_list == "string") {
                if (style_list == "Modern") {
                    list.push("0902")
                } else if (style_list == "Casual") {
                    list.push("0907")
                }
            } else {
                for (var i=0; i<style_list.length; i++) {
                    if (style_list[i] == "Modern") {
                        list.push("0902")
                    } else if (style_list[i] == "Casual") {
                        list.push("0907")
                    }
                }
            }
            console.log("list: ", list)
            OtUtil.send({"style_cds":list}, {
                url : '/req/sale/page/1?rowcnt=3',
                verbose : false,
                method : 'post',
                callback : {
                    success : function(data) {
                        // console.log("here!!!!!"); 
                        window.main_app.arrival_list = data.res;
                    }
                }
            });
        },
        
        loadCurationItem() {
            OtUtil.send({}, {
                url : '/req/man/recommend/page/1?rowcnt=30',
                verbose : false,
                method : 'post',
                callback : {
                    success : function(data) {
                        // console.log("here!!!!!"); 
                        var dataMap = data.res;
                        var list = [];
                        console.log(dataMap.topItemlist);
                        window.main_app.curation_list = list.concat(dataMap.topItemlist).concat(dataMap.outerItemlist).concat(dataMap.bottomItemlist);
                    }
                }
            });
        }
    },
    
    mounted(){
        //await this.fetch_arriavl_list(this.style_list)
        //await this.fetch_curation_list(this.style_list)

        // for(var i=0;i<3;i++){
        //     this.arrival_list.push({ 
        //         img_link: "http://mblogthumb1.phinf.naver.net/MjAxODAzMTFfMTkg/MDAxNTIwNzMxNjM4MjQ2.TH3pMELNPse1-qI9FHTsNywaj27NmoGdzh72LyhytYUg.5BsQu3NsczolitzUay1aGXJt0eZ4Wk7TIZcRhgNq_E4g.JPEG.wellduo33/IMG_2588.jpg?type=w800",
        //         sale_percent : "0",
        //         // new_rate: "1",
                
        //         style_main: "",
        //         style_time: "빈하루",
        //         item_name: "basic_rider jacket",
        //         item_price: "23000"

        //     })
        // }

        // for(i=0;i<10;i++){
        //     this.curation_list.push({
        //         img_link: "http://mblogthumb1.phinf.naver.net/MjAxODAzMTFfMTkg/MDAxNTIwNzMxNjM4MjQ2.TH3pMELNPse1-qI9FHTsNywaj27NmoGdzh72LyhytYUg.5BsQu3NsczolitzUay1aGXJt0eZ4Wk7TIZcRhgNq_E4g.JPEG.wellduo33/IMG_2588.jpg?type=w800",
        //         sale_percent : "0",
        //         // new_rate: "1",
                
        //         style_main: "",
        //         style_time: "빈하루",
        //         item_name: "basic_rider jacket",
        //         item_price: "23000"
        //     })
        // }

        this.loadArrivalItem(["0902","0907"]);
        this.loadCurationItem();
        this.loadSaleList();
        console.log(" 1111 axios saleproduct_list ::::: " + this.saleproduct_list);
    }
    
})
