
//css
import "@/css/animation.scss"
import "@/css/font_bundle.scss"
import "./curation.scss"
import "@/views/head/head.scss"

//third-party
import Vue from "@/js/vue"
import axios from "@/js/axios"
import Velocity from "velocity-animate"
//const
import env from "../../../config/env_property"
import dom from "@/js/prop/dom"

//components
import range from "@/js/components/range/range"
import cube from "@/js/components/cube/cube"

window.survey = new Vue({
    
    el: "#app",

    components:{
        range, cube
    },

    data: {
        stages: [["의상","0804"],["코디","0803"],["테마","0805"],"spec","alphago"],
        curr_stage: 0,
        curationing: true,

        query_list: ["당신이 평소에 자주 입는 의상을 하나만 골라주세요",
                     "코디 정말 중요하죠? 어떤 코디가 취향이신가요?",
                     "지금 필요한 옷이 있으신가요? 그 옷을 어떤 장소에서 입으실 거에요?",
                     "자~ 딱 하나 남았어요. 당신의 사이즈와 나이를 알려주세요",
                     "인공지능.. 가동중.."],
        
        selection_list: [],
        selection_data: {},

        superior_list: {},
        
        //input 
        shoe_list: [],
        detector: false,

        size_list: [245,250,255,260,265,270,275,280,285],


        //animation
        prev: {}
    },

    watch: {

    },

    computed: {
        //질문 문장 생성
        query(){
            return this.query_list[this.curr_stage]
        },
        //특정 리스트만 제공
        filtered_list(){
            return this.selection_list[this.stages[this.curr_stage][1]]
        },
        //셀력션 여부
        fullfill(){
            return this.filtered_list.some(({is_selected})=>is_selected)
        },

        edge_of_curation(){
            return this.stages.length - 3
        }
    },

    methods: {
        //push to superior
        select(event,selection){
            let selected = false
            Object.entries(this.selection_list).forEach(entry=>{
                if(entry[0]==this.stages[this.curr_stage][1]){
                    selected = entry[1].some(({is_selected})=>is_selected )
                }
            })
            if(!selected){
                selection.is_selected = true
                window.survey.selection_list[selection.style_type_cd].is_selected = true
                this.superior_list[this.stages[this.curr_stage][1]] = selection.style_cd
            }
        },

        next(){
            if(this.fullfill){
                if(this.curr_stage==this.edge_of_curation){
                    this.curationing = false
                    this.curr_stage++
                }else{
                    this.curr_stage++
                }
            }else{
                alert("한 가지를 선택해 주세요")
            }
        },
        
        compress({key,value}){
            this.superior_list[key] = value
        },

        with_alphago(){
            //dat collecting
            this.detector = true
            this.curr_stage++
            this.superior_list['shoes'] = this.shoe_list
            setTimeout(()=>{
                axios.request({
                    url:"/req/ai",
                    method: "post",
                    data: this.superior_list,
                    responseType: "json"
                }).then(({data})=>{
                    //setTimeout(()=>location.href="/ai_result",2000)                
                }).catch(err=>{
                    //setTimeout(()=>location.href="/ai_result",2000)
                })
            },2000)
        },


        //애니메이션
        before_enter(el){
            
        },
        enter(el){
            
        },

        before_leave(el){
            this.prev = el
        },
        leave(el,done){
            const duration = 1000
            Velocity(el,
                    {height:0, opacity:0, "margin-bottom":0},
                    {duration,
                     progress(ele,percent){
                         
                     }, 
                     complete(){el.style.display="none"}
                    })
            el.style.visibility= "hidden"
            
            scroll(0,dom.header.height)
        }

    },

    created(){
        //fetching style_list
        axios.request({
            url: "/req/style/list/",
            method: "post",
            responseType: "json"
        }).then(({data})=>{
            for(let [key,value] of Object.entries(data)){
                if(/^\d+$/.test(key)){
                    value.forEach(atom=>atom['is_selected']=false)
                }
            }
            this.selection_list = data
        })
        
        //spec init
        this.shoe_list.push(this.size_list[0])

    },
    mounted() {
        // console.log("여기타나요? 1111111111");
        // OtUtil.send(null, {
        //     url : '/req/style/list/',
        //     verbose : false,
        //     method : 'post',
        //     callback : {
        //         success : function(data) {
        //             // console.log("here!!!!!"); 
        //             console.log("여기타나요? 2222222222");
        //             // console.log(data);
        //             window.survey.selection_list = data;
        //         }
        //     }
        // });
    }

})