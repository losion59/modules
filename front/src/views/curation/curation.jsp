<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="옷딜, 모던, 남성의류쇼핑몰 베스트 모아서 내 사이즈로 자동 추천받자 ">
<meta name="keywords" content="옷딜, 남성쇼핑몰, 쇼핑앱, 쇼핑어플, 20대남성쇼핑몰">
<meta name="author" content="옷딜">
<!--property for opengraph-->
<meta property="og:image" content="https://www.otdeal.io/resources/mobile/images/common/icn/logo2.png" />

<title>옷딜</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/pc/male/common/head.jsp" flush="true"/>
    <div id="app">
        <transition 
            v-on:before-enter="before_enter"
            v-on:enter="enter"
            v-on:before-leave="before_leave"
            v-on:leave="leave">

            <div id="curation_container" class="container" ref="curation"
                 v-if="curationing" key="curation">
                <div id="logo">
                </div>
                <div id="title">
                    <span> OUTFIT<span>'</span>s Style Curation </span>
                </div>
                <div id="sub_line"></div>
                <div id="curation">
                    <div id="query"> {{ query }} </div>
                    <div id="selection_box">
                    <transition name="fade-list">
                        <div class="box" :class="stage"
                             v-for="(stage,idx) in stages" :key="stage+idx"
                             v-if="stage == stages[curr_stage]">
                            <div class="selection"
                                v-for="(selection,idx) in filtered_list" :key="selection.img_seq"
                                @click="select($event,selection)">
                                <img class="src"
                                    :src="selection.img_link">
                                <div class="trans_cover"
                                     v-if="selection['is_selected']"></div>
                            </div>
                        </div>
                    </transition>
                    </div>
                </div>
                <div id="next"
                    @click="next"> 더 알아 보기 </div>
            </div>
            
            <div id="spec_container"
                 class="container"
                 v-else key="spec">
                <div id="logo"></div>
                <div id="title">
                    <span> OUTFIT<span>'</span>s Style Curation </span>
                </div>
                <div id="sub_line"></div>
                <div id="query"> {{ query }} </div>
                <div id="spec">
                    <div class="tag"> 상의 사이즈 </div>
                    <range :min="80" :max="110" :step="5"
                           :audience="true" :detector="detector" :name="'shirts'"
                           @register="compress"></range>
                    <div class="tag"> 하의 사이즈 </div>
                    <range :min="28" :max="36" :step="1"
                           :audience="true" :detector="detector" :name="'pants'"
                           @register="compress"></range>
                    <div class="tag"> 신발 사이즈 </div>
                    <div id="cubes">
                        <cube v-for="size in size_list"
                              :width="'55px'" :height="'30px'" 
                              :spec="size" :king="shoe_list[0]"
                              @add="(value)=>shoe_list.push(value)" 
                              @remove="()=>shoe_list.pop()"
                              ></cube>
                    </div>
                    <div class="blank bord"></div>
                    <div class="tag"> 나이 </div>
                    <range :min="10" :max="50" 
                           :audience="true" :detector="detector" :name="'ages'"
                           @register="compress"
                           ></range>
                    <div class="blank"></div>
                    <div id="confirm"> 
                        <div @click.once="with_alphago"> 
                            <span>추천받기</span> 
                        </div>
                    </div>
                    <div id="loading"
                         v-if="stages[curr_stage]=='alphago'">
                        <img src="/resources/pc/img/loading.gif">
                        <div id="msg_box">
                            <div>인공지능이</div>
                            <div>결과를 분석중입니다.</div>
                        </div>
                    </div>
                </div>
            </div>
        </transition>    
    </div>
    <script type="text/javascript" src="/resources/common/js/common.js"></script>
</body>

</html>