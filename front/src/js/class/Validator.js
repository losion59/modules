const regExp = {
    PHONE: /\d{4}/,
    POSTCODE: "postcode",
    EMAIL: /[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}/,

    NAME: "nam"
}

const filter = {
    REQUIRED: "required"
}

export { regExp, filter }

export default (function(){
    function Validator(rules){
        
        this.regExps = []
        this.filters = []

        rules.forEach(rule => {
            if(/^\\\S*\\$/.test(rule)){
                this.regExps.push(new RegExp(rule, "g"))
            }else{
                this.filters.push(rule)
            }
        })

        //instance
        this[filter.REQUIRED] = function(identifier, value){   
            if(!value){
                return `${identifier} is required`
            }
            return ""
        }

        this.validate = (identifier, value)=>{
            let error_msg = ""
            
            this.regExps.forEach(regExp => {
                if(!regExp.test(value)){
                    error_msg = "${identifier} is not valid"
                    return
                }
            })
            this.filters.forEach(filter => {
                let error = this[filter](identifier, value)
                if(error){
                    error_msg = error
                    return 
                }
            })

            return error_msg
        }

    }

    //prototype
    

    return Validator;
})()