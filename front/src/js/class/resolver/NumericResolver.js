import "@babel/polyfill"

export default (function(){
   
   //private memeber
   const type_convert = Symbol("type_convert")
   
    class NumericResolver{
    
        static [type_convert](anything){
            if(typeof anything == "string"){
                return anything
            }else{
                return ""+anything
            }
        }
    
        static to_won(num){
            num =  this[type_convert](num)
            let num_buff = ""

            Array.from(num).reverse().forEach((char,index)=>{
                num_buff += char
                if((index+1)%3 == 0 && num.length!=index+1)num_buff += ","
            })
           
            let res_buff = ""
            Array.from(num_buff).reverse().forEach(char=>{
                res_buff += char
            })

            return res_buff
        }

        static to_num(won){
            won = this[type_convert](won)
            let won_buff = ""
            won.match(/\d+|[.]/g).forEach(char=>{
                won_buff += char
            })
            return won_buff
        }
    }

    return NumericResolver
})()