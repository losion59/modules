import "@babel/polyfill"
import NumResolver from "@class/resolver/NumericResolver"

export default class ArgumentResolver{

    consturctor(){
        
    }

    static to_list(anything){
        let list_buffer = []

        if(typeof anything == "object"){
            list_buffer = [...anything]
        }else{
            list_buffer.push(anything)
        }
        return list_buffer
    }

    static as_won_list(list){
        list.forEach((atom)=>{
            for(let [key,value] of Object.entries(atom)){
                if(/^\d+(원|[$]|\d)$/.test(value))atom[key] = NumResolver.to_won(value)
            }
        })
    }
    
}