
const iamport = {

    BUYER_NAME: "buyer_name",
    BUYER_TELEPHONE: "buyer_telephone",
    BUYER_EMAIL: "buyer_email",
    BUYER_ADDRESS: "buyer_address",
    BUYER_POSTCODE: "buyer_postcode"

}

export { iamport as imp }