package org.cern.exception;

public class PythonException extends Exception{
	public PythonException(String msg){
		super(msg);
	}
}
