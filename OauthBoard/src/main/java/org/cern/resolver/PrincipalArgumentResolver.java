package org.cern.resolver;

import java.lang.annotation.Annotation;

import org.cern.annotation.Principal;
import org.cern.security.parts.Man;
import org.springframework.core.MethodParameter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class PrincipalArgumentResolver implements HandlerMethodArgumentResolver{

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		for(Annotation annotation : parameter.getParameterAnnotations()) {
			if(annotation.getClass() == Principal.class) {
				return true;
			}
		}
		return true;
	}

	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {

		return (Man)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	
	
}
