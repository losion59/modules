package org.cern.util;

import java.util.HashMap;
import java.util.Map;

public class MapSpawn {
	public static Map<String, Object> bear(Object ...obj){
		HashMap<String,Object> reposit = new HashMap<>();
		for(int i=0; i<obj.length; i+=2) {
			try {
				reposit.put(obj[i].toString(), obj[i+1].toString());
			}catch(Exception exception){
				reposit.put(obj[i].toString(), null);
			}
		}
		return reposit;
	}
}
