package org.cern.util;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.ibatis.type.Alias;

@Alias("keymap")
public class LowerCaseHashmap extends LinkedHashMap<String, Object> {

	private static final long serialVersionUID = 6591174444703005035L;

	@Override
	public Object put(String key, Object value) {	
		return super.put(key.toLowerCase(), value);
	}
	
}
