package org.cern.controller;


import java.util.HashMap;
import java.util.Map;

import org.cern.annotation.Principal;
import org.cern.security.Security;
import org.cern.security.parts.Man;
import org.cern.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class NavigationController {
	
	@RequestMapping(value = {"/","/home"})
	public String toHome(@Principal Man man) {
		
		return "home";
	}
	
	@Autowired
	private ClientRegistrationRepository clientRegistrationRepository;
	@Autowired
	UserService service;
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping("/login")
	public String toLogin(Model model) {

		Iterable<ClientRegistration> clientRegistrations = null;
		ResolvableType type = ResolvableType.forInstance(clientRegistrationRepository).as(Iterable.class);
		if (type != ResolvableType.NONE && ClientRegistration.class.isAssignableFrom(type.resolveGenerics()[0])) {
			clientRegistrations = (Iterable<ClientRegistration>) clientRegistrationRepository;
		}

		Map<String, String> oauth2AuthenticationUrls = new HashMap<>();
		String authorizationRequestBaseUri = "oauth2/authorization";
		
		clientRegistrations.forEach(registration -> oauth2AuthenticationUrls.put(registration.getClientName(),
				authorizationRequestBaseUri + "/" + registration.getRegistrationId()));
		
		model.addAttribute("urls", oauth2AuthenticationUrls);
		model.addAttribute("login_processing_url", Security.STATE.LOGIN_PROCESSING_URL);
		model.addAttribute("name_parameter", Security.STATE.USERID);
		model.addAttribute("password_parameter", Security.STATE.USERPW);
		
		return "login";
	}
	
	@RequestMapping("/censor/{mode}/{position}")
	public String toCensor(@PathVariable String mode, 
						   @PathVariable(value="position",required = false) int position, Model model) {
		model.addAttribute("mode", mode);
		model.addAttribute("position", position);
		return "/censor";
	}
	
}
