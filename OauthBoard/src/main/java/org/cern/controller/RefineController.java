package org.cern.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.cern.App;
import org.cern.exception.PythonException;
import org.cern.service.RefineService;
import org.cern.util.MapSpawn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/unrefineded")
public class RefineController {
	
	private static Logger logger = LoggerFactory.getLogger(RefineController.class);
	
	@Autowired
	RefineService service;
	
	
	@RequestMapping(value = "/{start}/{limit}", method = RequestMethod.GET)
	public List<Map<String,Object>> fetch_unrefinededs(@PathVariable("start") int start,
													   @PathVariable("limit") int limit) throws Exception{
		return service.fetch_unrefinededs(MapSpawn.bear("start", start,
												 		"limit", limit));
	}
	
	@RequestMapping(value = "/{id_series}", method = RequestMethod.DELETE)
	public Boolean delete_unrefinededs(@PathVariable String id_series) {
		List<String> ids = Arrays.asList(id_series.split(","));
		service.delete_unrefinededs(ids);
		return true;
	}

	
	@RequestMapping(value="", method = RequestMethod.POST)
	public boolean insert_unrefinededs() throws IOException, InterruptedException, PythonException {
		return run_cmdline(get_cmd(App.SYSTEM_KEY.PYTHON_WEB_CRAWLER));
		
	}
	
	@RequestMapping(value="", method = RequestMethod.PATCH)
	public boolean patch_unrefinededs() throws IOException, InterruptedException, PythonException{	
		return run_cmdline(get_cmd(App.SYSTEM_KEY.PYTHON_EXCEL_PRINTER));	
	}
	
	
	
	@ExceptionHandler(value = {Throwable.class})
	public ResponseEntity<Map<String,Object>> print_error(Throwable e){
		return new ResponseEntity<>(MapSpawn.bear("message", e.getMessage()),
	    							HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	
	private boolean run_cmdline(String cmd) throws IOException, InterruptedException, PythonException {
		
		Process process = Runtime.getRuntime().exec(cmd);
	
		try {
			
		} finally {
			
			process.waitFor();
			InputStream error_input =  process.getErrorStream();
			if(error_input.available()!=0) {				
				BufferedReader cmd_input = new BufferedReader(new InputStreamReader(error_input));
				String readed_line = null;
				StringBuffer error_msg = new StringBuffer();
				while ((readed_line = cmd_input.readLine()) != null) {
					error_msg.append(readed_line);
					logger.error(readed_line);
				}
				throw new PythonException(error_msg.toString());
			}
		}
		return true;
	}
	
	
	@Autowired
	Environment env;
	
	private String get_cmd(String main) {
		String working_directory = env.getProperty(App.SYSTEM_KEY.PYTHON_WORKING_DIRECTORY);
		String main_module = env.getProperty(main);
		
		return String.format("/usr/local/bin/python3 %s --limit %s --host %s --username %s --password %s --db %s --sheet %s", 
						     working_directory+main_module,
						     700,
						     env.getProperty(App.SYSTEM_KEY.DB_HOST),
						     env.getProperty(App.SYSTEM_KEY.DB_USERNAME),
						     env.getProperty(App.SYSTEM_KEY.DB_PASSWORD),
						     env.getProperty(App.SYSTEM_KEY.DB_DATABASE),
						     "1140"); 
		
	}
}
