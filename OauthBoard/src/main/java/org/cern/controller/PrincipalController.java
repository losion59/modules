package org.cern.controller;

import java.util.HashMap;
import java.util.Map;

import org.cern.App;
import org.cern.exception.UserAlreadyExistException;
import org.cern.security.Security;
import org.cern.service.UserService;
import org.cern.util.MapSpawn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class PrincipalController {
	
	private static Logger logger = LoggerFactory.getLogger(PrincipalController.class);
	
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private PasswordEncoder encoder; 
	@Autowired
	private Environment env;
	
	@Autowired
	UserService user_service;
	
	@RequestMapping("/oauth/code/{provider}")
	public String publicoath(@RequestParam Map<String, String> params,
							 @PathVariable String provider, RedirectAttributes redirectAttr,
							 Model model) {
		final String redirect_url = env.getProperty(App.OAUTH_PREFIX+provider+".redirect-uri");
		final String client_id = env.getProperty(App.OAUTH_PREFIX+provider+".client-id");
		final String client_secret = env.getProperty(App.OAUTH_PREFIX+provider+".client-secret");
		final String granty_type = "authorization_code";
		final String code = params.get("code");
		final String token_endpoint = env.getProperty("oauth2."+provider+".token-endpoint");
		final String user_endpoint = env.getProperty("oauth2."+provider+".user-endpoint");
		
		Map<String,String> request_token = new HashMap<>();
		request_token.put("code", code);
		request_token.put("client_id", client_id);
		request_token.put("client_secret", client_secret);
		request_token.put("redirect_uri", redirect_url);
		request_token.put("grant_type", granty_type);
		
		Map<String,String> access_response = restTemplate.postForObject(token_endpoint, request_token, Map.class);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.AUTHORIZATION, access_response.get("token_type")+" "+access_response.get("access_token"));
		HttpEntity<?> httpEntity = new HttpEntity<>(headers);
		Map user_resources = restTemplate.exchange(user_endpoint, HttpMethod.GET, httpEntity, Map.class).getBody();
		
		Map<String,Object> user_info = MapSpawn.bear("userid", user_resources.get("id"),
													 "userpw", encoder.encode(App.PRIVATE_CODE+user_resources.get("email")),
													 "email", user_resources.get("email"),
													 "authorities", Security.Role.USER);
		//회원가입 처리
		try {
			user_service.registerUser(user_info);
		}catch(UserAlreadyExistException exception) {
			logger.error(exception.getMessage());
		}finally {
			user_info.replace("userpw", App.PRIVATE_CODE+user_resources.get("email"));
		}
			
		//form login 처리
		model.addAttribute("user", user_info);
		model.addAttribute("login_processing_url", Security.STATE.LOGIN_PROCESSING_URL);
		model.addAttribute("name_parameter", Security.STATE.USERID);
		model.addAttribute("password_parameter", Security.STATE.USERPW);
		
		return "bridge";
	}
	
}
