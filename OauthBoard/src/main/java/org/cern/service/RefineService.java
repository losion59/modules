package org.cern.service;

import java.util.List;
import java.util.Map;

import org.cern.dao.RefineDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RefineService {

	@Autowired
	RefineDAO dao;
	
	@Transactional
	public List<Map<String, Object>> fetch_unrefinededs(Map<String, Object> bear) {
		return dao.fetch_unrefinededs(bear);
	}

	public void delete_unrefinededs(List<String> ids) {
		dao.delete_unrefinededs(ids);		
	}

}

