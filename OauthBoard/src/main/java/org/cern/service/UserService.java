package org.cern.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.cern.dao.UserDAO;
import org.cern.exception.UserAlreadyExistException;
import org.cern.security.parts.Man;
import org.cern.util.MapSpawn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@SuppressWarnings("unchecked")
@Service
public class UserService implements UserDetailsService {

	@Autowired
	UserDAO user_dao;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		List<Map<String,Object>> user_list = user_dao.fetchUser(MapSpawn.bear("userid", username));
		
		if(user_list.size() == 0) {
			throw new UsernameNotFoundException("user was not founded");
		}
		
		Map<String,Object> user = user_list.get(0);
		
		
		
		String userid = user.get("userid").toString();
		String userpw = user.get("userpw").toString();
		String email = user.get("email").toString();
		
		Date createdAt = (Date)user.get("createdat");
		Date loginAt = (Date)user.get("loginat");
		
		List<String> authorities = Arrays.asList(user.get("authorities").toString());
		if(authorities == null)authorities = new ArrayList<>();
	
		return new Man(userid, userpw, authorities).setEmail(email)
												   .setCreatedAt(createdAt)
												   .setLoginAt(loginAt);
	}
	
	
	public void registerUser(Map<String,Object> user_info) throws UserAlreadyExistException{
		user_dao.registerUser(user_info);
	}

}
