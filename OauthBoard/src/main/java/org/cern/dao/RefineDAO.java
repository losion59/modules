package org.cern.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RefineDAO {
	
	private int toInt(Object value) {
		return Integer.parseInt(value.toString());
	}
	
	@Autowired
	SqlSessionTemplate template;

	public List<Map<String, Object>> fetch_unrefinededs(Map<String, Object> bear) {
		return template.selectList("fetch_unrefinededs",bear);
	}
	
	
	public void delete_unrefinededs(List<String> ids) {
		template.delete("delete_unrefinededs", ids);
	}
}
