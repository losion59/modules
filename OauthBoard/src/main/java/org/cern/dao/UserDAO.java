package org.cern.dao;

import java.util.List;
import java.util.Map;

import org.cern.exception.UserAlreadyExistException;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO {
	
	@Autowired
	SqlSessionTemplate template;
	
	public List<Map<String,Object>> fetchUser(Map<String,Object> param){
		return template.selectList("fetchUser", param);
	}

	public void registerUser(Map<String, Object> user_info) throws UserAlreadyExistException {
		if(isOrdinalUser(user_info)) {
			throw new UserAlreadyExistException("user is already here");
		}
		template.insert("registerUser", user_info);
	}
	
	private boolean isOrdinalUser(Map<String, Object> user_info) {
		return template.selectList("fetchUser", user_info).size()>0?true:false;
	}
	
}
