package org.cern;


import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class App {

	public static class SYSTEM_KEY{
		public static final String EXTRA_CONFIG = "spring.config.additional-location";
		
		public static final String PYTHON_WORKING_DIRECTORY = "python.project_directory";
		public static final String PYTHON_WEB_CRAWLER = "python.web_crawler";
		public static final String PYTHON_EXCEL_PRINTER = "python.excel_printer";
		
		public static final String DB_HOST = "spring.datasource.host";
		public static final String DB_USERNAME = "spring.datasource.username";
		public static final String DB_PASSWORD = "spring.datasource.password";
		public static final String DB_DATABASE = "spring.datasource.database";
		
	}
	
	
	public static final String DEV_CONFIG = "classpath:config/dev/" ;
	public static final String PROD_CONFIG = "classpath:config/prod/" ;
	
	public static final String OAUTH_PREFIX = "spring.security.oauth2.client.registration.";
	
	public static final String PRIVATE_CODE = "4SDF34SDHSD7";
	
	
	
	private static String concate(String ...strs) {
		StringBuffer str_buff = new StringBuffer();
		for(int i=0;i<strs.length;i++) {
			str_buff.append(strs[i]+",");
		}
		str_buff.deleteCharAt(str_buff.length()-1);
		return str_buff.toString();
	}
	
	public static Properties getEnvProperties() {
		Properties env_prop = new Properties();
		env_prop.put(SYSTEM_KEY.EXTRA_CONFIG, concate(DEV_CONFIG,PROD_CONFIG));
		return env_prop;
	}
	
	
	public static void main(String[] args) {
		new SpringApplicationBuilder().sources(App.class)
									  .properties(getEnvProperties())
									  .run(args);
		
	}

}
