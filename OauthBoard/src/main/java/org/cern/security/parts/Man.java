package org.cern.security.parts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

public class Man extends User {

	private static final long serialVersionUID = 9144350104466094869L;

	private String email;
	private Date loginAt;
	private Date createdAt;

	public String getEmail() {
		return email;
	}

	public Man setEmail(String email) {
		this.email = email;
		return this;
	}

	public Date getLoginAt() {
		return loginAt;
	}

	public Man setLoginAt(Date loginAt) {
		this.loginAt = loginAt;
		return this;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Man setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
		return this;
	}
	
	private static Collection<? extends GrantedAuthority> authorities(List<String> authorities){
		return authorities.stream().map((atom)->new SimpleGrantedAuthority(atom)).collect(Collectors.toList());
	}

	public Man(String username, String password, List<String> authorities) {	
		super(username, password, Man.authorities(authorities));
		
	}

}
