package org.cern.security.parts;

import java.nio.charset.StandardCharsets;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.google.common.hash.Hashing;

public class ShaPasswordEncoder implements PasswordEncoder{

	
	
	@Override
	public String encode(CharSequence rawPassword) {
		return Hashing.sha256().hashString(rawPassword, StandardCharsets.UTF_8).toString().toUpperCase();
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return encodedPassword.equals(encode(rawPassword));
	}
	
	
	
}
