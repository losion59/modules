package org.cern.security.parts;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cern.security.Security;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

public class AuthFailHandler implements AuthenticationFailureHandler {

	private static Logger logger = LoggerFactory.getLogger(AuthFailHandler.class);
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		logger.error(exception.getMessage());
		response.sendRedirect(Security.STATE.LOGIN_SUCCESS_URL);
	}

}
