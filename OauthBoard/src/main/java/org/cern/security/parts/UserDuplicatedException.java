package org.cern.security.parts;

import org.springframework.security.core.AuthenticationException;

public class UserDuplicatedException extends AuthenticationException {

	private static final long serialVersionUID = 1185229451325821315L;

	private static String default_msg = "something happend on duplicated registration";
	
	public UserDuplicatedException() {
		super(default_msg);
	}
	
	public UserDuplicatedException(String msg) {
		super(msg);
	}

}
