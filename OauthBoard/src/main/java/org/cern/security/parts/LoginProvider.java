package org.cern.security.parts;

import java.util.ArrayList;

import org.cern.controller.PrincipalController;
import org.cern.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;


public class LoginProvider implements AuthenticationProvider {

	@Autowired
	UserService user_service;
	@Autowired
	PasswordEncoder encoder;
	
	private static Logger logger = LoggerFactory.getLogger(LoginProvider.class);
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		
		String username = authentication.getName();
		String userpassword = authentication.getCredentials().toString();
		
		Man user = (Man)user_service.loadUserByUsername(username);
			
		logger.error(username);
		logger.error(userpassword);
		
		if(!encoder.matches(userpassword, user.getPassword())) {
			throw new BadCredentialsException("wrong password");
		}
		
		return new UsernamePasswordAuthenticationToken(user, encoder.encode(userpassword), user.getAuthorities());
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.isAssignableFrom(UsernamePasswordAuthenticationToken.class);
	}

}
