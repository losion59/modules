package org.cern.security;

import java.text.Format;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cern.security.parts.AuthFailHandler;
import org.cern.security.parts.AuthSuccessIndigator;
import org.cern.security.parts.LoginProvider;
import org.cern.security.parts.ShaPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyUtils;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.session.HttpSessionEventPublisher;


@Configuration
@EnableWebSecurity
public class Security extends WebSecurityConfigurerAdapter {
	
	


	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(loginProvider());
	}



	@Override
	public void configure(WebSecurity web) throws Exception {
		//web resources exception list
		web.ignoring()
			.antMatchers("/js/**")
			.antMatchers("/css/**")
			.antMatchers("/img/**");
		
		//registring handler
		web.expressionHandler(expressionHandler());
	}

	public static class STATE{

		public static final String LOGIN_PAGE = "/login";
		
		public static final String LOGIN_PROCESSING_URL = "/loginForm";
		public static final String LOGIN_SUCCESS_URL = "/home";
		public static final String LOGIN_FAIL_URL = "/login?error=fail";
		
		public static final String LOGOUT_PAGE = "/logout";
		public static final String ACCESS_DENIED_PAGE = "/error/reject";
		
		public static final String USERID = "usernmae";
		public static final String USERPW = "password";

	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.csrf().disable();
		
		http.authorizeRequests()
			.antMatchers("/login*","/bridge","/oauth/code/**").permitAll()
			.anyRequest().permitAll()
//			.and()
//				.addFilterBefore(, BasicAuthenticationFilter.class)
			.and()
				.oauth2Login()
				.loginPage(STATE.LOGIN_PAGE)
			.and()
				.formLogin()
				.usernameParameter(STATE.USERID)
				.passwordParameter(STATE.USERPW)
				.loginPage(STATE.LOGIN_PAGE)
				.loginProcessingUrl(STATE.LOGIN_PROCESSING_URL)
				.successHandler(authenticationSuccessHandler())
				.failureHandler(authenticationFailureHandler())
			.and()
				.logout()
				.logoutUrl(STATE.LOGOUT_PAGE)
				.logoutSuccessUrl(STATE.LOGIN_SUCCESS_URL)
			.and()
				.exceptionHandling()
				.accessDeniedPage(STATE.ACCESS_DENIED_PAGE)
			.and()
				.sessionManagement()
				.maximumSessions(1)
				.maxSessionsPreventsLogin(true)
				.sessionRegistry(sessionRegistry())
				.expiredUrl(STATE.LOGIN_PAGE)
				;
	}
	
	

	//Session publish Listener
	@Bean
	public ServletListenerRegistrationBean<HttpSessionEventPublisher> servletListenerRegistrationBean(){
		return new ServletListenerRegistrationBean<>(new HttpSessionEventPublisher());
	}
	
	//password encoder
	@Bean
	public PasswordEncoder shaPasswordEncoder() {
		return new ShaPasswordEncoder();
	}
	
	//auth provider alpha
	@Bean
	public LoginProvider loginProvider() {
		return new LoginProvider();
	}
	//auth  handler
	@Bean
	public AuthenticationSuccessHandler authenticationSuccessHandler() {
		return new AuthSuccessIndigator();
	}
	@Bean
	public AuthenticationFailureHandler authenticationFailureHandler() {
		return new AuthFailHandler();
	}
	
	//session repository
	@Bean
	public SessionRegistry sessionRegistry() {
		return new SessionRegistryImpl();
	}
	
	public static class Role{
		public static final String ADMIN = "ROLE_ADMIN";
		public static final String MANAGER = "ROLE_MANAGER";
		public static final String USER = "ROLE_USER";
	}
	
	@Bean
	public RoleHierarchy roleHierarchy() {
		String role_hierarchy = String.format("%s > %s > %s", Role.ADMIN, Role.MANAGER, Role.USER);
		RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
		roleHierarchy.setHierarchy(role_hierarchy);
		return roleHierarchy;
	}
	
	@Bean
	public SecurityExpressionHandler<FilterInvocation> expressionHandler(){
		DefaultWebSecurityExpressionHandler default_handler = new DefaultWebSecurityExpressionHandler();
		default_handler.setRoleHierarchy(roleHierarchy());
		return default_handler;
	}
	
}
