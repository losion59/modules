import "./censor.scss"

import Vue from "@/js/vue"
import axios from "@/js/axios"

import "@babel/polyfill"
import "@/js/jquery"

import row from "./component/row"

const bus = new Vue({
    data: {
        pointer_init : 0
    }
})
window.bus = bus

const container = new Vue({
    
    el : "#list_container",

    components:{
        row
    },

    
    data : {
        limit : 20,
        pointer : 0,

        fetched_list : [],
        delete_list : [],

        crawling_status : "크롤링하기",
        fetching_status : "데이터 가져오기",
        printing_status : "출력하기",

        warn: false,
        src: "",

        mode: "",
        position: 0
    },

    methods:{
        
        start_crawling(){
            this.crawling_status = "pending"
            let promise =axios.post(`/unrefineded`)
                 .then(()=>{
                    this.crawling_status = "크롤링하기"
                 })
                 .catch(err=>alert(err))
            return promise
        },

        fetch_unrefineded(){
            this.fetching_status = "pending"
            
            bus.pointer_init = this.pointer            
            let unrefinded_list = axios.request({
                    url: `/unrefineded/${this.pointer}/20`,
                    method : "get"
                }).then(res=>{
                    this.fetching_status = "데이터 가져오기"
                    this.fetched_list = res.data
                    this.warn=false
                    this.$refs.console.value = ""
                })
                
            return unrefinded_list
        },

        print_refineded(){
            this.printing_status = "pending"
            axios.request({
                url: `/unrefineded`,
                method: "patch"
            }).then(()=>{
                this.printing_status = "출력하기"
            })
        },

        get_side(idx){
            return (idx<10)?"left":"right"
        },
        as_console(){
            this.$refs.console.focus()
        },
        has_on_delete(atom){
            return this.delete_list.some(id=>(id==atom)?true:false)
        },
        show_big_one(src){
            this.src = src
        },

        push(event){
            switch(event.key){
                case "a": this.next();break
                case "z": this.previouse();break
                case "s": this.discard();break
                case "r": this.revoke();break
                case "d": this.yield();break
                case "f": this.unyield();break
                //case "v": this.discard_all();break
            }
            this.as_console()
        },

        move(no){
            this.pointer = no
            this.as_console()
        },
        next(){
            if(!this.is_on_boarder){
                this.pointer = ++this.pointer
            }else{
                this.warn=true
            }
        },
        previouse(){
            this.pointer = --this.pointer
        },
        discard(){
            if(!this.is_on_boarder){
                this.delete_list.push(this.pointer)
                this.pointer = ++this.pointer
            }else{
                if(!this.has_on_delete(this.pointer)){
                    this.delete_list.push(this.pointer)
                }
                this.warn=true
            }
        },
        revoke(){
            let curr_idx = this.delete_list.indexOf(this.pointer)
            if (curr_idx > -1){
                this.delete_list.splice(curr_idx, 1)
            }
        },

        async yield(){
            //request delete
            let dels = ""
            this.delete_list.forEach((atom)=>dels+=atom+",")
            if(!dels)dels="10000"
            await axios.request({
                url: `/unrefineded/${dels.substring(0, dels.length-1)}`,
                method: "delete"
            })
            //pointer move
            this.pointer = bus.pointer_init + this.limit
            //clear
            this.fetched_list = []
            this.delete_list = []
            this.fetch_unrefineded()

        },

        async unyield(){
            this.pointer = bus.pointer_init - this.limit
            this.fetched_list = []
            this.delete_list = []
            this.fetch_unrefineded()
        },

        async discard_all(){
            let def = Number.parseInt(bus.pointer_init)
            while(def < def+Number.parseInt(this.limit)){
                this.delete_list.push(def)
                def += 1
            }
        },
        
    },

    computed: {
        is_on_boarder(){
            return (this.pointer+1==bus.pointer_init+this.limit)?true:false
        },
        is_right(){
            let right = false;
            try{
                if(this.pointer<Number.parseInt(this.fetched_list[20/2]["no"]))right = true
            }catch(error){
                console.log(error)
            }
            if(this.fetched_list.length<11)right=true
            
            return right
        }
    },

    created(){
        this.mode = $("#mode").val()
        this.position = $("#position").val()
        
    },
    
    mounted(){
        if(this.mode!="solo"){
            this.pointer = this.pointer + 20 * this.position
            switch(this.mode){
                case "duo": this.limit = this.limit*2;break
                case "trio": this.limit = this.limit*3;break
            }
            
        }
    }
})

window.container = container

/*
OrderedDict([('Sheet1',       

      번호   상품명                                              이미지경로  ... -- ---  -
0      1  조각케익  http://m.tastyfood.co.kr/web/product/big/20180...  ...  1   1  1
1      2    도넛  https://post-phinf.pstatic.net/MjAxODA0MDRfNiA...  ...  1   1  1
2      3    도넛  https://previews.123rf.com/images/tycoon751/ty...  ...  1   1  1
3      4    도넛  https://img.hankyung.com/photo/201804/01.16523...  ...  1   1  1
4      5    도넛  http://mblogthumb1.phinf.naver.net/MjAxNzAzMjF...  ...  1   1  1
..   ...   ...                                                ...  ... ..  .. ..
396  397    옷딜  http://img.otdeal.net/item/2017/12/29/74607c20...  ...  1   1  1
397  398    옷딜  http://img.otdeal.net/item/2017/12/29/2a998e48...  ...  1   1  1
398  399    옷딜  http://img.otdeal.net/item/2017/12/29/feb6c9d5...  ...  1   1  1
399  400    옷딜  http://img.otdeal.net/item/2017/12/29/28ea7941...  ...  1   1  1
400  401    옷딜  http://img.otdeal.net/item/2017/12/29/49584b19...  ...  1   1  1

[401 rows x 16 columns])])


Index(['번호', 'Sheet1_가중치', 'Sheet1', '감성-커피', '뷰티-모델-셀카', '음식-디저트', '패션-옷딜상품',
       '--', '---', '-'],
      dtype='object')

*/