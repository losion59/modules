import axios from "@/js/axios"
import "@babel/polyfill"


export default {

    template: ` <div :class="{discarded: is_discarded, 
                              focus: is_focused}"
                     @click="move">
                    <div class="no">{{ no }}</div>
                    <div class="category">{{ category }}</div>
                    <div class="product_name">{{ product_name }}</div>
                    <div class="url">
                        <img :src="url" ref="img">
                    </div>
                </div>`,

    props: ["no", "product_name", "category","url",
            "pointer", "delete_list"],

    data(){
        return{
            
        }
    },
    
    methods:{
        move(){
            this.$emit("rapid", this.no)
        }
    },
    
    computed: {
        is_focused(){
            if(this.pointer==this.no){
                this.$emit('magnifiy', this.url)
                return true
            }
            return false
        },
        is_discarded(){
            return(this.delete_list.some((code)=>(code==this.no)?true:false))?true:false
        }
    },
    
    mounted(){
        //Object.defineProperty(window.document, "referer", {get : ()=> this.url})
        // axios.request({
        //     method: "post",
        //     url: this.url,
        //     headers : {
        //         "user-agent" : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
        //         ,"Referer" : this.url
        //     },
        //     responseType: "blob"
        // }).then(res=>{
        //     console.log(this.$ref)
        //     this.$ref.src = res.data
        // }).catch(err=>{
        //     console.log(err)
        // })
        
    }
}