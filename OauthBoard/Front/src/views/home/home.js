import './home.scss';

import '@/js/iamport';
import '@/js/jquery';

import Vue from '@/js/vue';
import { imp } from '@/js/prop/payment';

import validatedInput from './component/validatedInput';
import methodList from './component/methodList';
import payment from './component/payment';


const pay_module = new Vue({
    el: "#pay_module",

    components: {
        validatedInput,
        methodList,
        payment
    },
    
    data: {
        pg_group: ["inicis", "kakao", "danal"],
        pay_method_group: ["samsung", "card", "trans", "vbank", "phone"],

        selected: {
            pg: "",
            pay_method: ""
        },

        pay_form: {
            [imp.BUYER_NAME]: "",
            [imp.BUYER_TELEPHONE]: "",
            [imp.BUYER_EMAIL]: "",
            [imp.BUYER_ADDRESS]: "",
            [imp.BUYER_POSTCODE]: ""
        }
    },

    computed: {

    },

    methods: {
        form_update(buyer_info){
            console.log(buyer_info);
        },
        selected_update(selected_info){
            console.log(selected_info);
        }
    },
    
    created(){
        
    }
})


window.pay_module = pay_module;



// IMP.init('imp45370778');  
// IMP.request_pay({
//     pg : 'inicis', // version 1.1.0부터 지원.
//     pay_method : 'card',
//     merchant_uid : 'merchant_' + new Date().getTime(),
//     name : '주문명:결제테스트',
//     amount : 100,
//     buyer_email : 'iamport@siot.do',
//     buyer_name : '구매자이름',
//     buyer_tel : '010-1234-5678',
//     buyer_addr : '서울특별시 강남구 삼성동',
//     buyer_postcode : '123-456',
//     m_redirect_url : 'https://www.yourdomain.com/payments/complete'
// }, function(rsp) {
//     if ( rsp.success ) {
//         var msg = '결제가 완료되었습니다.';
//         msg += '고유ID : ' + rsp.imp_uid;
//         msg += '상점 거래ID : ' + rsp.merchant_uid;
//         msg += '결제 금액 : ' + rsp.paid_amount;
//         msg += '카드 승인번호 : ' + rsp.apply_num;
//     } else {
//         var msg = '결제에 실패하였습니다.';
//         msg += '에러내용 : ' + rsp.error_msg;
//     }

// });