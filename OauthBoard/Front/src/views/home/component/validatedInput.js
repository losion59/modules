
import validations from '@/js/directives/validation'

const validated_input = {

    directives: validations(),

    template: ` 
                <div>
                    <label :for="identifier"> {{ label }} </label>
                    <input :name="identifier" :id="identifier"
                           v-model="value" v-validate="filter_chain">
                    <span v-if="error" id="error"> {{ error }} </span>
                </div>
              `,

    props: ["identifier", "label", "filter_chain"],

    data(){    
        return{
            value: "",
            error: ""
        }
    },

    computed: {

    },
    
    methods: {
        inspect(){
            //유효성 검사
            //이벤트 발송
            this.$emit('form_done', this.$attr);
        }
    }

}

export default validated_input;