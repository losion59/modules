import { imp } from '@/js/prop/payment'

const pay_form = {

    template: ` <form class="payForm"
                    @blur="inspect">
                    <input type="text" id="${imp.BUYER_NAME}" v-model="${imp.BUYER_NAME}"><br>
                    <input type="text" id="${imp.BUYER_TELEPHONE}" v-model="${imp.BUYER_TELEPHONE}"><br>
                    <input type="text" id="${imp.BUYER_EMAIL}" v-model="${imp.BUYER_EMAIL}"><br>
                    <input type="text" id="${imp.BUYER_ADDRESS}" v-model="${imp.BUYER_ADDRESS}"><br>
                    <input type="text" id="${imp.BUYER_POSTCODE}" v-model="${imp.BUYER_POSTCODE}"><br>
                </form> `,

    props: [],

    data(){  

        const buyer_info_vo = { };
        Object.keys(imp).forEach((key)=>buyer_info_vo[imp[key]]=""); 
        
        return buyer_info_vo;
    },

    computed: {

    },
    
    methods: {
        inspect(){
            //유효성 검사
            //이벤트 발송
            this.$emit('form_done', this.$attr);
        }
    }

}

export default pay_form;