
const payment = {

    template:  `<div class="payment"
                    :id="pg"
                    @click="$emit('payment_done', pg)">
                    {{ pg }}
                </div>`,
    data(){
        return {}
    },
    props: ['pg'],
    
    computed: {
        
    },
    
    methods: {

    }

}

export default payment;