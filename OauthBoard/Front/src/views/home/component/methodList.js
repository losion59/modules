
const mothod_list = {

    template: ` <div class=methodList>
                    <template v-for="pay_method in pay_method_group">
                        <label> {{ pay_method }} </label>
                        <input  type="radio" name="pay_method"
                                :value="pay_method"
                                @click="$emit('method_done', $event.target.value)">
                    </template>
                </div>          
               `,
    data(){ 
        return {}; 
    },
    props: ["pay_method_group"],
    
    computed: {
        
    },
    
    methods: {

    }

}

export default mothod_list;