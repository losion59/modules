//PLUGINS
const HtmlWebpackPlugin = require('html-webpack-plugin');
//PROP
const env = require('./env_property');

const HTML = ({id, chunks, extension="html"})=>{
    if(!chunks)chunks=[id];
    const HTML_DEFAULT = { 
        hash: true, 
        inject: 'body',
        xhtml: true,

        chunks: ['vendor', ...chunks],
        filename: `view/${id}.${extension}`,
        template: env.VIEWS(id, extension)
    };

    return new HtmlWebpackPlugin(HTML_DEFAULT);
};

module.exports=[
    HTML({ id: 'home'}),
    HTML({ id: 'login' }),
    HTML({ id: 'censor' })
]