//PROP
const env = require('./env_property');

module.exports={
    "home" : [env.VIEWS("home","js")],
    "login" : [env.VIEWS("login","js")],
    "censor" : [env.VIEWS("censor","js")]
}