const path = require('path')

exports.CONTEXT_PATH="/"
exports.SRC_PATH=path.resolve(__dirname, "../src")
exports.VIEWS_PATH=this.SRC_PATH+"/views"
exports.DIST_PATH=path.resolve(__dirname, "../../src/main/resources/templates/")

exports.VIEWS=(road,ext)=>`${this.VIEWS_PATH}/${road}/${road}.${ext}`

